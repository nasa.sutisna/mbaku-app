export const environment = {
  production: true,
  apiUrl : 'https://api.mbaku.online/api/v1/',
  apiUrlStorage: 'https://api.mbaku.online/api/v1/storage/'
};
