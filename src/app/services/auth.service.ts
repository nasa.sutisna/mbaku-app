import { Injectable, EventEmitter, Input, Output } from '@angular/core';
import { RestApiService } from './rest-api.service';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  @Output() auth = new EventEmitter<any>();
  @Output() navbar = new EventEmitter<void>();

  role = new Subject();
  isLogin:boolean;
  constructor(
    public restApi: RestApiService,
    public router: Router,
  ) { }

  login(criteria){
    return new Promise((resolve,reject) => {
      this.restApi.processLogin(criteria).subscribe((result) => {
        this.isLogin = true;
        this.role.next({role: result.user.userStatus});
        resolve(result);
      },error => {
        reject(error);
      })
    })
  }
}
