import { Injectable } from '@angular/core';
import { RestApiService } from './rest-api.service';

@Injectable({
  providedIn: 'root'
})
export class StoragePathService {

  constructor(
    public restApi: RestApiService
  ) { }

  storageLibraryPhoto() {
    let path = this.restApi.apiUrlStorage + 'library/';
    return path;
  }

  storageBookCover(image: any, libraryID: any) {
    let path = this.restApi.apiUrlStorage + 'bookcover/' + libraryID + '/' + image;
    return path;
  }

  storageEbook(libraryID: any) {
    return new Promise((resolve) => {
      let path = this.restApi.apiUrlStorage + 'ebook/' + libraryID + '/';
      resolve(path);
    })
  }

  storageEbookCover(image: any, libraryID: any) {
    let path = this.restApi.apiUrlStorage + 'ebookcover/' + libraryID + '/' + image;
    return path;
  }

  storageProfile(memberID: any) {
    return this.restApi.apiUrlStorage + 'profile/' + memberID + '/';
  }
}
