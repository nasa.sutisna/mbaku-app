import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Platform } from '@ionic/angular';
import { pagination, getBanner, allowedRead, ICheckRole, IBookLoan, ILibrarySetting, IResult, IResponseMidtrans, ICheckPaymentEbook, loginInfo } from '../interface/interface';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class RestApiService {

  setAuth = new Subject();
  header: any = new HttpHeaders().append('Content-Type', 'application/json');
  typePdf: any = new HttpHeaders().append('Content-Type', 'application/pdf');
  isServer = 'local';

  // LINK MIDTRANS
  urlTransaction = 'https://app.sandbox.midtrans.com/snap/v1/transactions';
  authTransaction = "Basic U0ItTWlkLXNlcnZlci1JWlRnMUxMWTlGQjl0VF9JcXV6LWt3em46";

  // URL ENDPOINT
  apiUrl: string = environment.apiUrl;
  apiUrlStorage: string = environment.apiUrlStorage;

  constructor(public http: HttpClient, public platform: Platform) {
    this.setAuth.subscribe((data) => {
      let auth: any = data;
      let token = auth.token.access_token;
      this.header = new HttpHeaders().set('Authorization', 'Bearer ' + token).append('Content-Type', 'application/json');
    });

    this.initialize();
  }

  initialize() {
    let getAuth = localStorage.getItem('authentication');
    let auth = JSON.parse(getAuth);
    if (auth) {
      let token = auth.token.access_token;
      this.header = new HttpHeaders().set('Authorization', 'Bearer ' + token).append('Content-Type', 'application/json');
    }
  }

  showImage(pathImage: string) {
    if (pathImage) {
      let path = pathImage.substring(pathImage.lastIndexOf('public') + 6);
      return this.apiUrlStorage + 'storage/' + path;
    }

  }

  getBookList(criteria: any) {
    return this.http.post<pagination<any>>(this.apiUrl + `book/getBookList`, JSON.stringify(criteria), { headers: this.header });
  }

  getEbookList(criteria: any) {
    return this.http.post<pagination<any>>(this.apiUrl + `ebook/getEbookList`, JSON.stringify(criteria), { headers: this.header });
  }

  searchTitleBook(keyword: any) {
    return this.http.get<pagination<any>>(this.apiUrl + `book/searchTitle?keyword=${keyword}`, { headers: this.header });
  }

  searchTitleEbook(keyword: any) {
    return this.http.get<pagination<any>>(this.apiUrl + `ebook/searchTitle?keyword=${keyword}`, { headers: this.header });
  }

  getDetailBook(id) {
    return this.http.get(this.apiUrl + `book/detail/${id}`, { headers: this.header });
  }

  getDetailEbook(id) {
    return this.http.get(this.apiUrl + `ebook/detail/${id}`, { headers: this.header });
  }

  registerAccount(criteria: any) {
    return this.http.post(this.apiUrl + `register`, JSON.stringify(criteria), { headers: this.header });
  }


  registerAnggota(criteria: any) {
    return this.http.post(this.apiUrl + `anggota/daftar`, JSON.stringify(criteria), { headers: this.header });
  }

  getNearbyLibrary(criteria: any) {
    return this.http.post(this.apiUrl + `library/getNearby`, JSON.stringify(criteria), { headers: this.header });
  }

  getBanner(memberID: any) {
    return this.http.get<getBanner<any>>(this.apiUrl + `userBanner/${memberID}`, { headers: this.header });
  }

  getCategory(formData: any) {
    return this.http.post<pagination<any>>(this.apiUrl + `category/getCategory`, formData, { headers: this.header });
  }

  getListLibrary(formData: any) {
    return this.http.post<pagination<any>>(this.apiUrl + `library/getListLibrary`, formData, { headers: this.header });
  }

  getDetailLibrary(libID: any) {
    return this.http.get<pagination<any>>(this.apiUrl + `library/detail/${libID}`, { headers: this.header });
  }

  orderEbook(criteria: any) {
    return this.http.post(this.apiUrl + 'payment/ebook', criteria, { headers: this.header });
  }

  addFeedBack(criteria: any) {
    return this.http.post(this.apiUrl + 'ebook/addFeedBack', criteria, { headers: this.header });
  }

  checkMyFeedBack(criteria: any) {
    return this.http.post(this.apiUrl + 'ebook/checkMyFeedBack', criteria, { headers: this.header });
  }

  getFile(filename: string, libraryID: any) {
    return this.http.post(this.apiUrl + 'ebook/getEbook', { filename: filename, libraryID: libraryID }, { headers: this.header, responseType: 'blob' });
  }

  getDetailMember(id) {
    return this.http.get(this.apiUrl + `member/detail/${id}`, { headers: this.header });
  }

  saveEbookPayment(criteria) {
    return this.http.post(this.apiUrl + 'payment/ebook/save', criteria, { headers: this.header });
  }

  checkAccessRead(criteria) {
    return this.http.post<allowedRead>(this.apiUrl + 'ebook/checkAccessRead', criteria, { headers: this.header });
  }

  saveUpgradePremium(criteria) {
    return this.http.post(this.apiUrl + 'member/upgrade', criteria, { headers: this.header });
  }

  checkMemberStatus(criteria) {
    return this.http.post<ICheckRole>(this.apiUrl + 'member/checkMemberStatus', criteria, { headers: this.header });
  }


  memberSaveSaldo(criteria) {
    return this.http.post(this.apiUrl + 'member/saveSaldo', criteria, { headers: this.header });
  }

  memberUpdateProfile(criteria) {
    return this.http.post(this.apiUrl + 'member/updateProfile', criteria, { headers: this.header });
  }

  getUserInfo(criteria) {
    return this.http.post(this.apiUrl + 'user/getInfo', criteria, { headers: this.header });
  }

  topUpPendingPayment(id) {
    return this.http.post(this.apiUrl + `member/getStatusPayment`, { id: id }, { headers: this.header });
  }

  getEbookWishlist(id) {
    return this.http.get(this.apiUrl + `ebookWishlist/user/${id}`, { headers: this.header });
  }

  getEbookRental(id) {
    return this.http.get(this.apiUrl + `ebookRental/user/${id}`, { headers: this.header });
  }

  /**
   *  LOGIN
   */
  processLogin(criteria: any) {
    return this.http.post<loginInfo<any>>(this.apiUrl + `login`, JSON.stringify(criteria), { headers: this.header });
  }

  forgotPassword(criteria: any) {
    return this.http.post(this.apiUrl + `forgot/password`, criteria, { headers: this.header });
  }

  resetPassword(criteria: any) {
    return this.http.post(this.apiUrl + `reset/password`, criteria, { headers: this.header });
  }


  /**
    *  SALDO E-WALLATE
    */
  logSaldo(criteria) {
    return this.http.post<pagination<any>>(this.apiUrl + `logSaldo`, criteria, { headers: this.header });
  }

  /**
   * TRANSACTIONS LOAN
   */
  getLoanHistory(criteria) {
    return this.http.post(this.apiUrl + `loanHistory/user`, criteria, { headers: this.header });
  }

  getBookLoan(id) {
    return this.http.get<IBookLoan>(this.apiUrl + `bookLoan/user/${id}`, { headers: this.header });
  }

  getMemberBookLoan(libararyID,memberID) {
    return this.http.get<IBookLoan>(this.apiUrl + `loan/getMemberBookLoan/${libararyID}/${memberID}`, { headers: this.header });
  }

  loanGetBook(libararyID,bookID) {
    return this.http.get(this.apiUrl + `loan/getBook/${libararyID}/${bookID}`, { headers: this.header });
  }

  loanTransaction(criteria) {
    return this.http.post(this.apiUrl + `loanTransaction`, criteria, { headers: this.header });
  }

  returnTransaction(criteria) {
    return this.http.post(this.apiUrl + `returnTransaction`, criteria, { headers: this.header });
  }

  /**
   * GET SETTING LIBRARY
   */
  getSetting(id) {
    return this.http.get<ILibrarySetting>(this.apiUrl + `setting/library/${id}`, { headers: this.header });
  }

  updateSetting(criteria) {
    return this.http.post<IResult>(this.apiUrl + `setting/library`, criteria, { headers: this.header });
  }

  /**
   * PAYMENT
   */
  getStatusPayment(criteria) {
    return this.http.post<IResponseMidtrans>(this.apiUrl + `member/getStatusPayment`, criteria, { headers: this.header });
  }

  checkPendingPaymentEbook(criteria) {
    return this.http.post<ICheckPaymentEbook>(this.apiUrl + `payment/ebook/checkPendingPaymentEbook`, criteria, { headers: this.header });
  }

  updatePaymentEbook(criteria) {
    return this.http.post(this.apiUrl + `payment/ebook/updatePaymentEbook`, criteria, { headers: this.header });
  }

  memberTopUpSaldo(criteria) {
    return this.http.post(this.apiUrl + 'member/topUpSaldo', criteria, { headers: this.header });
  }

  paymentTopUp(criteria) {
    return this.http.post(this.apiUrl + 'payment/topup', criteria, { headers: this.header });
  }

  paymentTopUpUpdate(criteria) {
    return this.http.post(this.apiUrl + 'payment/updatetopup', criteria, { headers: this.header });
  }

  checkPendingPaymentTopUp(criteria) {
    return this.http.post<ICheckPaymentEbook>(this.apiUrl + 'payment/checkPendingPaymentTopUp', criteria, { headers: this.header });
  }

  /**
   * ADMIN
   */
  getDataDashboardAdmin(libraryID){
    return this.http.get(this.apiUrl + `library/dashboard/${libraryID}`,{headers: this.header});
  }


}
