import { Injectable } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class NavService {

    data: any;

    constructor(
        public navCtrl: NavController,
        public toastCtrl: ToastController
    ) {
        // ...
    }

    push(url: string, data: any = '') {
        this.data = data;

        this.navCtrl.navigateForward('/' + url);
    }

    pop(url) {
        this.navCtrl.navigateBack('/' + url);
    }

    setRoot(url: string, data: any = '') {
        this.data = data;

        this.navCtrl.navigateRoot('/' + url);
    }

    get(key: string) {
        return this.data[key];
    }

    showToast(message, post: any = "bottom") {
        this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: post
        }).then((toastData) => {
            console.log(toastData);
            toastData.present();
        });
    }
}
