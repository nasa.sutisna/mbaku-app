import { TestBed } from '@angular/core/testing';

import { StoragePathService } from './storage-path.service';

describe('StoragePathService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StoragePathService = TestBed.get(StoragePathService);
    expect(service).toBeTruthy();
  });
});
