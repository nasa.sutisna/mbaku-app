import { Injectable, EventEmitter } from '@angular/core';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { File } from '@ionic-native/file/ngx';
import { Platform, ToastController, LoadingController } from '@ionic/angular';
import { FileTransferObject, FileTransfer,FileUploadOptions } from '@ionic-native/file-transfer/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { Subject } from 'rxjs';
import { RestApiService } from './rest-api.service';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  loading: any;

  filterBook = new Subject();
  filterEbook = new Subject();
  sorting = new Subject();
  navigate:EventEmitter<any> = new EventEmitter();
  
  private win: any = window;
  constructor(
    public androidPermissions: AndroidPermissions,
    public file: File,
    public platform: Platform,
    public transfer: FileTransfer,
    public toastCtrl: ToastController,
    public fileOpener: FileOpener,
    public loadingCtrl: LoadingController,
    private restApi: RestApiService
  ) { }

  pathImage(image) {
    if (this.platform.is('cordova')) {
      // handle new version wkWebView version >= 3
        return this.win.Ionic.WebView.convertFileSrc(image);
    }
    else {
      return image;
    }
  }

  async upload(memberID,imageData,filename, remoteMethod) {
   return new Promise((resolve,reject) => {
    const auth = JSON.parse(localStorage.getItem('authentication'));
    let options: FileUploadOptions = {
      fileKey: 'photo',
      fileName: filename,
      chunkedMode: false,
      headers : {
        "Authorization": `Bearer ${auth.token.access_token}`
      },
      params: {
        memberID: memberID
      }
   }
   
   const fileTransfer: FileTransferObject = this.transfer.create();
   const pathUrl = this.restApi.apiUrl + remoteMethod;

   fileTransfer.upload(imageData, pathUrl, options)
    .then((data) => {
      console.log(data);
      resolve(JSON.parse(data.response).filename);
      // success
    }, (err) => {
      reject(err)
     console.log(err);
      // error
    })
   })
  }

  download(url, imageOriginalName): Promise<any> {
    return new Promise((resolve, reject) => {
      let path = '';
      this.setPathDownload().then(() => {
        var fileName = imageOriginalName;
        fileName = fileName.split(" ").join("_"); // for handle if fileName exp. imageName date blabla.jpg change to ImageName_date_blabla.jpg

        if (this.platform.is('android')) {
          path = this.file.externalRootDirectory + "Download/MBAKU/" + fileName;
        }
        else if (this.platform.is('ios')) {
          path = this.file.documentsDirectory + "Download/MBAKU/" + fileName;
        }

        const fileTransfer: FileTransferObject = this.transfer.create();
        fileTransfer.download(url, path, true)
          .then((entry) => {
            let filePath: string = entry.toURL();
            console.log(filePath);
            this.fileOpener.open(filePath, 'application/pdf').then(results => { console.log(results) }).catch(e => console.log(e));
            this.showToast('Successfully Download');
            resolve({ filePath: filePath });
          }, (error) => {
            this.showToast('Maaf, ada sesuatu yang salah');
            reject({ filePath: "", error: error });
          });
      }).catch(error => {
        reject({ filePath: "", error: error });
        console.log('set path error', error);
      });
    })

  }

  setPathDownload(): Promise<any> {
    if (this.platform.is('android')) {
      return this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(
        success => {
          if (success.hasPermission) {
            return this.file.checkDir(this.file.externalRootDirectory, 'Download/MBAKU').catch(err => {
              return this.file.createDir(this.file.externalRootDirectory, 'Download/MBAKU', true)
            })
          } else {
            return this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(rest => {
              return this.file.checkDir(this.file.externalRootDirectory, 'Download/MBAKU').catch(err => {
                return this.file.createDir(this.file.externalRootDirectory, 'Download/MBAKU', true)
              })
            })
          }
        });
    } else if (this.platform.is('ios')) {
      return this.file.checkDir(this.file.documentsDirectory, 'Download/MBAKU').catch(err => {
        console.log('check path error', err);
        return this.file.createDir(this.file.documentsDirectory, 'Download/MBAKU', true).catch(err => {
          console.log('create path error', err);
        })
      })
    } else {
      return Promise.reject({
        message: 'Error Platform..'
      });
    }
  }

  showToast(message, post: any = "bottom", duration:number = 5000) {
    this.toastCtrl.create({
      message: message,
      duration: duration,
      position: post,
      showCloseButton: true,
      closeButtonText: 'OK'
    }).then((toastData) => {
      console.log(toastData);
      toastData.present();
    });
  }

  async presentLoading(message = 'Loading...') {
    this.loading = await this.loadingCtrl.create({
      message: message
    });
    await this.loading.present();
  }

  async dismissLoading() {
    return await this.loadingCtrl.dismiss().then(() => console.log('dismissed'));
  }

  formatRupiah(angka, prefix = 'Rp.') {
    if (angka) {
      let number_string = angka.toString().replace(/[^,\d]/g, ''),
        split = number_string.split(','),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);

      // tambahkan titik jika yang di input sudah menjadi angka ribuan
      if (ribuan) {
        let separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }

      rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
      return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah + ',-' : '');
    }

    return 0;
  }

  getYear(item) {
    if (item) {
      return item.substring(0, 4);
    }

    return '';
  }

}
