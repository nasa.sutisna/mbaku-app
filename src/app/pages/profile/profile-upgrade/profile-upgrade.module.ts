import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProfileUpgradePage } from './profile-upgrade.page';

const routes: Routes = [
  {
    path: '',
    component: ProfileUpgradePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProfileUpgradePage]
})
export class ProfileUpgradePageModule {}
