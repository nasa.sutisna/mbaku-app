import { Component, OnInit } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { GlobalService } from 'src/app/services/global.service';
import { ActivatedRoute } from '@angular/router';
import { RestApiService } from 'src/app/services/rest-api.service';
import { NavController, Events } from '@ionic/angular';

@Component({
  selector: 'app-profile-upgrade',
  templateUrl: './profile-upgrade.page.html',
  styleUrls: ['./profile-upgrade.page.scss'],
})
export class ProfileUpgradePage implements OnInit {

  multipleImage: any = [];

  formData = {
    memberID: null,
    emergencyNumber: null,
    emergencyName: null,
    emergencyRole: null,
    memberPhotoKTP1: null,
    memberPhotoKTP2: null,
  }

  imgKtp1: any;
  imgKtp2: any;
  memberID: any;

  constructor(
    private camera: Camera,
    public globalService: GlobalService,
    private route: ActivatedRoute,
    private restApi: RestApiService,
    private navCtrl: NavController,
    public events: Events
  ) {
    this.route.queryParams.subscribe((params) => {
      this.memberID = params['memberID'];
      this.formData.memberID = this.memberID;
    })
  }

  ngOnInit() {
  }

  openCamera() {
    const options: CameraOptions = {
      quality: 70,
      targetHeight: 400,
      targetWidth: 400,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }

    this.camera.getPicture(options).then((imageData) => {
      this.imgKtp1 = imageData;
    }, (err) => {
      // Handle error
    });
  }

  openCamera2() {
    const options: CameraOptions = {
      quality: 70,
      targetHeight: 400,
      targetWidth: 400,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }

    this.camera.getPicture(options).then((imageData) => {
      this.imgKtp2 = imageData;
    }, (err) => {
      // Handle error
    });
  }

  async saveForm() {
    this.globalService.presentLoading('Mengirim data...');

    try {
      let ktp1 = await this.globalService.upload(this.memberID, this.imgKtp1, 'ktp1', 'member/uploadPhotoKTP');
      let ktp2 = await this.globalService.upload(this.memberID, this.imgKtp2, 'ktp2', 'member/uploadPhotoKTP');

      this.formData.memberPhotoKTP1 = ktp1;
      this.formData.memberPhotoKTP2 = ktp2;

      console.log(this.formData);

      this.restApi.saveUpgradePremium(this.formData).subscribe((result) => {
        this.globalService.showToast('Pengajuan berhasil, mohon tunggu paling lambat 1 x 24 jam. Terima Kasih');
        this.globalService.dismissLoading();
        this.events.publish('MEMBER:UPDATEPROFILE', true);
        this.navCtrl.back();
      }, err => {
        console.log(err);
        this.globalService.showToast('Gagal mengirim data');
        this.globalService.dismissLoading();
      })
    }
    catch (err) {
      console.log(err);
      this.globalService.showToast('Gagal mengirim data');
      this.globalService.dismissLoading();
    }
  }

  back() {
    this.navCtrl.back();
  }

}
