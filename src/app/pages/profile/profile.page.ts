import { Component, OnInit, NgZone } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { RestApiService } from '../../services/rest-api.service';
import { AuthService } from '../../services/auth.service';
import { GlobalService } from '../../services/global.service';
import { NavController, Events, Platform } from '@ionic/angular';
import { StoragePathService } from 'src/app/services/storage-path.service';
import { HttpErrorResponse } from '@angular/common/http';
declare const snap: any;
@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {


  user: any;
  formData = new FormGroup({});
  file: any;
  image: any;
  segment: string = 'book';
  path: any;
  memberRole: any;
  submission: string;
  ebookWishList: any = [];
  bookHistory: any = [];
  bookLoan: any = [];
  ebookRental: any = [];
  ebookRentalTotal: number = 0;
  bookLoanTotal: number = 0;
  isLoadingRental: boolean;
  isLoadingLoan: boolean;
  isLoadingWhiteList: boolean;
  intervalCheck: any;
  intervalCheckUserInfo: any;

  paramsOrder = {
    full_name: '',
    email: '',
    phone: '',
    ebookID: '',
    ebookTitle: '',
    ebookPrice: ''
  }

  isLoading: boolean = true;

  constructor(
    public fb: FormBuilder,
    public restApi: RestApiService,
    public authService: AuthService,
    public globalService: GlobalService,
    public storagePath: StoragePathService,
    public navCtrl: NavController,
    public events: Events,
    public ngZone: NgZone,
    public platform: Platform
  ) { }

  ngOnInit() {
    this.getDetailData();
    this.handleEvents();

    this.platform.pause.subscribe(() => {
      this.unsubEvents();
    })

    this.platform.resume.subscribe(() => {
      this.handleEvents();
    })
  }

  ngOnDestroy(): void {
    this.unsubEvents();
  }

  getUserInfo() {
    let auth = JSON.parse(localStorage.getItem('authentication'));
    const params = {
      memberID: auth.user.userInfo.memberID
    }

    this.restApi.getUserInfo(params)
      .subscribe((result: any) => {
        if (result.data) {

          this.ngZone.run(() => {
            this.user = result.data;
            this.memberRole = Number(this.user.memberRole);
            this.submission = this.user.memberApprovalText;
            this.memberRole = this.user.memberRole
          })

          if (this.user.isAlreadyAlertTrans == 0) {
            this.getBookLoan();
            this.events.publish('UPDATE:BANNER', true)
            this.globalService.showToast('Transaksi berhasil');
          }
        }
      }, (error: HttpErrorResponse) => {
        if (error.status == 401) {
          this.globalService.showToast('Tidak Teraunthentikasi');
          this.logout();
        }
      })
  };

  handleEvents() {
    this.events.subscribe('MEMBER:UPDATEPROFILE', () => {
      this.getDetailData();
    })

    this.intervalCheckUserInfo = setInterval(() => {
      this.getUserInfo();
    }, 5000);
  }

  unsubEvents() {
    this.events.unsubscribe('MEMBER:UPDATEPROFILE');

    for (let i = 0; i <= this.intervalCheck; i++) {
      clearInterval(i);
    }

    for (let i = 0; i <= this.intervalCheckUserInfo; i++) {
      clearInterval(i);
    }
  }

  getDetailData() {
    let auth = JSON.parse(localStorage.getItem('authentication'));

    this.isLoading = true;
    this.isLoadingRental = true;
    this.isLoadingLoan = true;
    this.isLoadingWhiteList = true;

    this.restApi.getDetailMember(auth.user.userInfo.memberID)
      .subscribe((result: any) => {

        this.ngZone.run(() => {
          this.user = result;
          this.path = this.storagePath.storageProfile(result.memberID);
          localStorage.setItem('user', JSON.stringify(result));
        })

        this.isLoadingRental = false;
        this.isLoadingLoan = false;
        this.isLoadingWhiteList = false;
        this.isLoading = false;

        this.checkMemberStatus();
        this.getEbookWishlist();
        this.getBookLoan();
        this.getRentalEbook();
      }, err => {
        this.isLoadingRental = false;
        this.isLoadingLoan = false;
        this.isLoadingWhiteList = false;
        this.isLoading = false;
        this.globalService.showToast(err.error.msg);
      });
  }

  checkMemberStatus() {
    this.restApi.checkMemberStatus({ memberID: this.user.memberID }).subscribe((result) => {
      this.memberRole = result.memberRole;
      this.submission = result.submission;
    }, error => {
      console.log(error);
    })
  }

  logout() {
    localStorage.removeItem('authentication');
    localStorage.removeItem('user');
    this.authService.isLogin = false;
    this.navCtrl.navigateRoot("/page-init");
  }

  gotoScanner() {
    this.navCtrl.navigateForward(['/transactions']);
  }

  generateQRCode() {
    this.navCtrl.navigateForward(['/profile-qrcode'], { queryParams: { memberID: this.user.memberID } })
  }

  upgradeMember() {
    this.navCtrl.navigateForward(['/profile-upgrade'], { queryParams: { memberID: this.user.memberID } });
  }

  async openSnap() {
    let auth: any = JSON.parse(localStorage.getItem('user'));

    this.paramsOrder.full_name = auth.memberFirstName + ' ' + auth.memberLastName;
    this.paramsOrder.email = auth.memberEmail;
    this.paramsOrder.phone = auth.memberPhone;

    this.globalService.presentLoading();

    this.restApi.orderEbook(this.paramsOrder).subscribe(async (result: any) => {

      snap.pay(result.token, {
        gopayMode: 'deeplink',
        onSuccess: (response) => {
          console.log('sukses', response);
        },
        onPending: (response) => {
          console.log('sukses 2', response);
          if (response.payment_type != 'gopay') {
          }
        },
        onError: (response) => {
          console.log(response);
        },
        onClose: () => {
          console.log('customer closed the popup without finishing the payment');
        }
      });

      this.globalService.dismissLoading();
    }, error => {
      this.globalService.dismissLoading();
    });

  }

  topUp() {
    this.navCtrl.navigateForward(['/profile-top-up'], { queryParams: { id: this.user.memberID } });
  }

  getEbookWishlist() {
    this.restApi.getEbookWishlist(this.user.memberID).subscribe((result: any) => {
      this.ebookWishList = result.ebook;
      this.isLoadingWhiteList = false;
    }, error => {
      this.isLoadingWhiteList = false;
      console.log(error);
    })
  }

  getBookLoan() {
    this.restApi.getBookLoan(this.user.memberID).subscribe((result: any) => {
      this.ngZone.run(() => {
        this.bookLoan = result.book;
        this.bookLoanTotal = result.bookTotal
      })
    }, error => {
      console.log(error);
    })
  }

  getRentalEbook() {
    this.restApi.getEbookRental(this.user.memberID).subscribe((result: any) => {
      this.ebookRental = result.ebook;
      this.ebookRentalTotal = result.ebookTotal
      this.isLoadingRental = false;
    }, error => {
      console.log(error);
      this.isLoadingRental = false;
    })
  }

  gotoDetailEbook(ebookID, ebook) {
    this.navCtrl.navigateForward(['/ebook-detail'], { queryParams: { ebookID: ebookID, ebook: ebook } })
  }

  gotoDetailBook(bookID) {
    this.navCtrl.navigateForward(['book-detail'], { queryParams: { bookID: bookID } });
  }
}
