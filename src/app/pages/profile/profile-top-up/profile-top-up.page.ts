import { Component, OnInit } from '@angular/core';
import { RestApiService } from 'src/app/services/rest-api.service';
import { GlobalService } from 'src/app/services/global.service';
import { ActivatedRoute } from '@angular/router';
import { Events, NavController, ActionSheetController, Platform } from '@ionic/angular';
import { AppAvailability } from '@ionic-native/app-availability/ngx';
import { ActionSheetOptions } from '@ionic/core';
import { InAppBrowserOptions, InAppBrowser } from '@ionic-native/in-app-browser/ngx';

declare const snap;

@Component({
  selector: 'app-profile-top-up',
  templateUrl: './profile-top-up.page.html',
  styleUrls: ['./profile-top-up.page.scss'],
})
export class ProfileTopUpPage implements OnInit {

  formData = {
    page: 1,
    limit: 20,
    id: null,
    role: 'member'
  }

  params = {
    full_name: '',
    email: '',
    phone: '',
    nominal: null,
    type: 'topup',
    paymentType: '',
    memberID: null
  }

  historyList: Array<any> = [];
  gojekExist: boolean = false;
  paymentPending: boolean = false;
  isOpenGojek: boolean = false;
  gojekToken: any;
  intervalCheck: any;
  userData: any;

  constructor(
    private restApi: RestApiService,
    private route: ActivatedRoute,
    public globalService: GlobalService,
    private events: Events,
    private navCtrl: NavController,
    public actionSheetController: ActionSheetController,
    public platform: Platform,
    public appAvailability: AppAvailability,
    public inAppBrowser: InAppBrowser,
  ) { }

  ngOnInit() {
    this.userData = JSON.parse(localStorage.getItem('user'));

    this.platform.pause.subscribe((data) => {

    });

    this.platform.resume.subscribe((data) => {
      if (this.isOpenGojek) {
        this.getPaymentStatus(this.gojekToken);
      }
    })

    this.route.queryParams.subscribe((params) => {
      this.params.memberID = params['id'];
      this.formData.id = this.params.memberID
      this.getHistory();
      this.checkAppAvailability();
      this.checkStatusPending()
    })
  }

  ngOnDestroy(): void {
    clearInterval(this.intervalCheck);
    this.isOpenGojek = false;
  }

  getPaymentStatus(token) {
    const params = {
      id: token
    }

    this.restApi.getStatusPayment(params).subscribe((result: any) => {
      if (result.transaction_status == 'settlement') {
        this.saveSaldo(result.token, result);
      }
    })
  }

  checkAppAvailability() {
    if (this.platform.is('cordova')) {
      const app = 'com.gojek.app';
      this.appAvailability.check(app)
        .then((result) => {
          this.gojekExist = result;
        });
    }
  }

  checkStatusPending() {
    const params = {
      memberID: this.userData.memberID
    }

    this.restApi.checkPendingPaymentTopUp(params).subscribe((result) => {
      this.paymentPending = result.paymentPending;
      if (result.paymentPending) {
        this.intervalCheck = setInterval(() => {
          this.checkPaymentPending(result.paymentToken);
        }, 10000)
      }
    })
  }

  getHistory() {
    this.restApi.logSaldo(this.formData).subscribe((result) => {
      this.historyList = result.data
    })
  }

  async presentActionSheet() {

    if(this.params.nominal < 10000){
      this.globalService.showToast('Minimal Top Up Rp. 10.000,-');
      return false;
    }

    let options: ActionSheetOptions = {
      header: 'Metode Pembayaran',
      buttons: []
    }

    options.buttons.push(
      {
        text: 'GOPAY',
        icon: 'radio-button-on',
        handler: () => {
          if (this.gojekExist) {
            this.openSnap('gopay');
          }
          else {
            this.globalService.showToast('Aplikasi Gojek tidak ditemukan di perangkat kamu');
          }
        }
      }, {
        text: 'ATM/Bank Transfer',
        icon: 'card',
        handler: () => {
          this.openSnap('bank_transfer');
        }
      },
      {
        text: 'Batalkan',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }
    )

    const actionSheet = await this.actionSheetController.create(options);
    await actionSheet.present();
  }

  async openSnap(paymentType) {
    let auth: any = JSON.parse(localStorage.getItem('user'));

    if (this.paymentPending) {
      this.globalService.showToast('Kamu sudah melakukan request topup tapi belum melunasi pembayaran', 'bottom', 10000);
      return false;
    }

    this.checkAppAvailability();

    this.params.full_name = auth.memberFirstName + ' ' + auth.memberLastName;
    this.params.email = auth.memberEmail;
    this.params.phone = auth.memberPhone;
    this.params.paymentType = paymentType;

    this.globalService.presentLoading();


    this.restApi.memberTopUpSaldo(this.params).subscribe(async (result: any) => {

      if (paymentType == 'bank_transfer') {
        this.bankTransfer(result.token)
      }
      else {
        this.gopay(result.redirect_url, result.token);
      }

      this.globalService.dismissLoading();
    }, error => {
      this.globalService.dismissLoading();
    });

  }

  bankTransfer(token) {
    snap.pay(token, {
      enabledPayments: ["bca_va", "permata_va", "bni_va", "echannel", "other_va"],

      onSuccess: (response) => {
        this.saveSaldo(token, response);
      },
      onPending: (response) => {
        if (response.payment_type != 'gopay') {
          this.saveSaldo(token, response);
        }
      },
      onError: (response) => {
        console.log(response);
      },
      onClose: () => {
        console.log('customer closed the popup without finishing the payment');
      }
    });
  }

  gopay(redirect_url, token) {

    const options: InAppBrowserOptions = {
      location: 'no',
      hidenavigationbuttons: 'yes',
      toolbar: 'no',
      hardwareback: 'yes',
      fullscreen: 'yes'
    }

    const browser = this.inAppBrowser.create(redirect_url, '_blank', options)

    if (this.platform.is('cordova')) {
      browser.on("loadstop")
        .subscribe(response => {
          console.log('response iab', response);
          let loopDeeplink = setInterval(() => {
            let url = response.url;
            if (url.indexOf('gojek://gopay') > -1) {
              browser.close();
              clearInterval(loopDeeplink);
              if (this.gojekExist) {
                this.openDeeplink(url);
                this.isOpenGojek = true;
                this.gojekToken = token;
              }
              else {
                this.globalService.showToast('Aplikasi Gojek tidak ditemukan di perangkat kamu');
              }
            }

          }, 200);
        });
    }
  }

  openDeeplink(deeplink) {
    const browser = this.inAppBrowser.create(deeplink, '_system', 'location=no');
    browser.on("loadstop")
      .subscribe(response => {
        console.log('response iab', response);
        let loopDeeplink = setInterval(() => {
          console.log('response iab', response);
          clearInterval(loopDeeplink)
        }, 500);
      });
  }

  checkPaymentPending(token) {
    const params = {
      id: token
    }

    this.restApi.getStatusPayment(params).subscribe((result: any) => {
      console.log(result);
      if (result.transaction_status == 'settlement') {
        this.updatePaymentTopUp(result)
      }
    })
  }

  saveSaldo(token, params) {
    this.globalService.presentLoading();

    let criteria = {
      memberID: this.userData.memberID,
      payment_token: token
    }

    criteria = Object.assign(criteria, params);
    console.log('criteria', criteria);

    this.restApi.paymentTopUp(criteria).subscribe((result) => {
      console.log(result);
      if (params.payment_type == 'bank_transfer' && params.transaction_status == 'pending') {
        this.globalService.showToast('Mohon check email kamu untuk melakukan pembayaran', 'bottom', 5000);
        this.events.publish('MEMBER:RUNCHECKPENDINGPAYMENT', (token));
      }
      else {
        this.globalService.showToast('TopUp Saldo Berhasil', 'bottom', 5000);
        this.events.publish('MEMBER:UPDATEPROFILE', true);
      }

      this.navCtrl.navigateBack(['/tabs/profile']);
      this.globalService.dismissLoading();
    }, error => {
      this.globalService.dismissLoading();
    })
  }

  updatePaymentTopUp(params) {
    const key = {
      transaction_token: params.token
    }

    params = Object.assign(params, key)
    this.restApi.updatePaymentEbook(params).subscribe((result) => {
      this.events.publish('MEMBER:TOPUPSUCCESS');
    });
  }


  back() {
    this.navCtrl.back();
  }
}
