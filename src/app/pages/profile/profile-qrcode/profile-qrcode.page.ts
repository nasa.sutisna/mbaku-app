import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile-qrcode',
  templateUrl: './profile-qrcode.page.html',
  styleUrls: ['./profile-qrcode.page.scss'],
})
export class ProfileQrcodePage implements OnInit {

  memberID:any;
  constructor(
    private navCtrl: NavController,
    public route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      this.memberID = params.memberID;
    })
  }

  goBack(){
    this.navCtrl.navigateBack("/tabs/profile")
  }
}
