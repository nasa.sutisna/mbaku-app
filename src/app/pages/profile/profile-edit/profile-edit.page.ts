import { Component, OnInit } from '@angular/core';
import { NavController, Platform, Events } from '@ionic/angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { RestApiService } from 'src/app/services/rest-api.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';
import { StoragePathService } from 'src/app/services/storage-path.service';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.page.html',
  styleUrls: ['./profile-edit.page.scss'],
})
export class ProfileEditPage implements OnInit {

  formData = {
    memberID: null,
    firstName: null,
    lastName: null,
    phone: 0,
    address: null,
  }

  photo: any;
  tempPhoto: any;
  memberID: any;

  constructor(
    public navCtrl: NavController,
    public transfer: FileTransfer,
    public restApi: RestApiService,
    public router: ActivatedRoute,
    public camera: Camera,
    public globalService: GlobalService,
    public storagePath: StoragePathService,
    public platform: Platform,
    public events: Events
  ) { }

  ngOnInit() {
    this.router.queryParams.subscribe((params) => {
      this.memberID = params.id;
      this.formData.memberID = this.memberID
      this.getDetail();
    })
  }

  getDetail() {
    this.restApi.getDetailMember(this.memberID)
      .subscribe((result:any) => {
        this.formData.firstName = result.memberFirstName
        this.formData.lastName = result.memberLastName
        this.formData.phone = result.memberPhone

        const path = this.storagePath.storageProfile(this.memberID);
        this.photo = path + result.memberPhoto
        console.log(result);
        
      })
  }

  getPicture() {
   if(this.platform.is('cordova')){
    const options: CameraOptions = {
      quality: 75,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }

    this.camera.getPicture(options).then((imageData) => {
      this.tempPhoto = imageData;
      this.photo = this.globalService.pathImage(this.tempPhoto);
    }, (err) => {
      // Handle error
    });
   }
  }

  save() {
    const auth = JSON.parse(localStorage.getItem('authentication'));
    const filename = 'IMG_' + this.memberID + '_' + new Date().getTime();

    this.globalService.presentLoading();
    let options: FileUploadOptions = {
      fileKey: 'photo',
      fileName: filename,
      chunkedMode: false,
      headers : {
        "Authorization": `Bearer ${auth.token.access_token}`
      },
      params: {
        memberID: this.memberID,
        firstName: this.formData.firstName,
        lastName: this.formData.lastName,
        phone: this.formData.phone
      }
    }

    if (this.tempPhoto) {
      const fileTransfer: FileTransferObject = this.transfer.create();
      const pathUrl = this.restApi.apiUrl + 'member/updateProfile';

      fileTransfer.upload(this.tempPhoto, pathUrl, options)
        .then((data) => {
          this.globalService.showToast('Profil berhasil diupdate');
          this.events.publish('MEMBER:UPDATEPROFILE',true);
          this.navCtrl.back();
          console.log(data);
          this.globalService.dismissLoading();
          // success
        }, (err) => {
          this.globalService.showToast('Maaf, ada sesuatu yang salah');
          this.globalService.dismissLoading();
          console.log(err);
          // error
        })
    }
    else {
      console.log('tanpa update photo');

      this.restApi.memberUpdateProfile(this.formData)
        .subscribe((result) => {
          this.globalService.showToast('Profil berhasil diupdate');
          this.events.publish('MEMBER:UPDATEPROFILE',true);
          this.globalService.dismissLoading();
          this.navCtrl.back();
        }, error => {
          this.globalService.showToast('Maaf, ada sesuatu yang salah');
          this.globalService.dismissLoading();
        })
    }
  }

  back() {
    this.navCtrl.back();
  }
}
