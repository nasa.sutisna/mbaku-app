import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { RestApiService } from 'src/app/services/rest-api.service';
import { GlobalService } from 'src/app/services/global.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile-history',
  templateUrl: './profile-history.page.html',
  styleUrls: ['./profile-history.page.scss'],
})
export class ProfileHistoryPage implements OnInit {

  historyList: Array<any> = [];
  formData =  {
    memberID: null,
    page:1,
    limit: 20
  }

  totalPage:any;
  page:any;

  constructor(
    public navCtrl: NavController,
    public restApi: RestApiService,
    public globalService: GlobalService,
    public route: ActivatedRoute
  ) {

   }

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      this.formData.memberID = params.id
      this.getHistory()
    })
  }

  gotoDetail(bookID, book) {
    this.navCtrl.navigateForward(['book-detail'], { queryParams: { bookID: bookID, book: book } });
  }
  
  getHistory(){
    this.restApi.getLoanHistory(this.formData).subscribe((result:any) => {
      this.historyList = result.book
    })
  }

  doInfinite(event, page) {
    this.formData.page = page + 1;
    this.restApi.getLoanHistory(this.formData).subscribe((result:any) => {
      this.totalPage = result.totalPage;
      this.page = result.page;
      this.formData.page = this.page;

      if (result.data.length > 0) {
        for (let item of result.data) {
          this.historyList.push(item);
        }
      }

      event.target.complete();
    }, err => {
      event.target.complete();
    })
  }

  back(){
    this.navCtrl.back()
  }
}
