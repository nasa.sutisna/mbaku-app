import { Component, OnInit, ViewChild } from '@angular/core';
import { NavService } from '../../../services/nav.service';
import { RestApiService } from '../../../services/rest-api.service';
import { AuthService } from '../../../services/auth.service';
import { NavController, IonContent } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { StoragePathService } from 'src/app/services/storage-path.service';
import { GlobalService } from 'src/app/services/global.service';
declare const window;

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.page.html',
  styleUrls: ['./book-detail.page.scss'],
})
export class BookDetailPage implements OnInit {
  @ViewChild(IonContent) content: IonContent;

  bookItem: any;
  bookID: any;
  bookList: any = [];
  segment1: any = 'description';
  formData = {
    page: 1,
    limit: 6,
    category: [],
    filter: null,
    keyword: '',
    sortBy: '',
    filterFromHome: '',
  }

  isLoading: boolean;
  isLoadingList: boolean;

  constructor(
    public navService: NavService,
    public restApi: RestApiService,
    public authService: AuthService,
    public navCtrl: NavController,
    public route: ActivatedRoute,
    public storagePath: StoragePathService,
    public globalService: GlobalService
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      this.bookID = params['bookID'];
      this.getDetailBook(this.bookID);
    })
  }

  getDetailBook(bookID) {
    this.isLoading = true;
    this.isLoadingList = true;
    this.restApi.getDetailBook(bookID).subscribe((result) => {
      this.bookItem = result;
      this.getBookList();
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
      this.isLoadingList = false;
    })
  }

  async getBookList() {
    this.formData.filter = {
      librarySelected: { libraryID: this.bookItem.libraryID },
      categorySelected: { categoryID: this.bookItem.categoryID }
    }
    this.restApi.getBookList(this.formData).subscribe(async (results: any) => {
      this.bookList = results.data;
      this.isLoadingList = false;
    },
      err => {
        this.isLoadingList = false;
      })
  }

  gotoEbook() {
    if (this.authService.isLogin) {
      this.navService.push("ebook", { kode_buku: this.bookItem.bookID, book: this.bookItem });
    }
    else {
      this.navCtrl.navigateForward("login");
    }
  }

  detail(bookItem) {
    this.globalService.presentLoading();
    this.bookItem = bookItem;
    setTimeout(() => {
      this.ScrollToTop();
      this.globalService.dismissLoading();
    }, 500)
  }

  segmentChanged(ev: any) {
    console.log('Segment changed', ev);
  }

  ScrollToTop() {
    this.content.scrollToTop(1000);
  }

  back() {
    this.navCtrl.back();
  }
}
