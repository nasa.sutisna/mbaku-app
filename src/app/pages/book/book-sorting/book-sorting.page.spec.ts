import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookSortingPage } from './book-sorting.page';

describe('BookSortingPage', () => {
  let component: BookSortingPage;
  let fixture: ComponentFixture<BookSortingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookSortingPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookSortingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
