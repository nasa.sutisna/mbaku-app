import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/services/global.service';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-book-sorting',
  templateUrl: './book-sorting.page.html',
  styleUrls: ['./book-sorting.page.scss'],
})
export class BookSortingPage implements OnInit {

  selected:any;

  constructor(
    public globalService: GlobalService,
    public route: ActivatedRoute,
    public navCtrl: NavController
  ) { }

  ngOnInit() {   
    this.route.queryParams.subscribe(params => {
    this.selected = params['sortBy'];
  });

  }

  applySorting(sortBy:string){
    this.globalService.sorting.next(sortBy);
    this.back()
  }

  reset(){
    this.globalService.sorting.next('');
    this.back()
  }

  back(){
    this.navCtrl.back();
  }
}
