import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInput, IonSearchbar, NavController } from '@ionic/angular';
import { RestApiService } from 'src/app/services/rest-api.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-book-search-title',
  templateUrl: './book-search-title.page.html',
  styleUrls: ['./book-search-title.page.scss'],
})
export class BookSearchTitlePage implements OnInit {

  @ViewChild('searchInput') searchInput: IonInput;
  searchControl = new FormControl();
  titleList = [];
  total: number = 0;
  totalPage: number = 0;
  page: number = 1;
  keyword: string = "";

  constructor(
    public restApi: RestApiService,
    public navCtrl: NavController
  ) { }

  ngOnInit() {
    setTimeout(() => {
      this.searchInput.setFocus();
    }, 400);
  }

  search() {
    if(this.keyword != ''){
      this.restApi.searchTitleBook(this.keyword)
      .subscribe(result => {
        this.titleList = result.data;
        this.page = result.page;
        this.totalPage = result.totalPage;
      })
    }
    else{
      this.titleList = [];
    }
  }

  gotoBook(keyword){
    console.log('keyword',keyword)
    this.navCtrl.navigateForward(["/book"],{queryParams: {keyword: keyword}});
  }


}
