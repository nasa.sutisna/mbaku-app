import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { BookSearchTitlePage } from './book-search-title.page';

const routes: Routes = [
  {
    path: '',
    component: BookSearchTitlePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BookSearchTitlePage]
})
export class BookSearchTitlePageModule {}
