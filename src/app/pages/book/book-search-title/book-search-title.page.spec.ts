import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookSearchTitlePage } from './book-search-title.page';

describe('BookSearchTitlePage', () => {
  let component: BookSearchTitlePage;
  let fixture: ComponentFixture<BookSearchTitlePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookSearchTitlePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookSearchTitlePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
