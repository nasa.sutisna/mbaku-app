import { Component, OnInit } from '@angular/core';
import { NavController, Events } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-filter-book',
  templateUrl: './filter-book.page.html',
  styleUrls: ['./filter-book.page.scss'],
})
export class FilterBookPage implements OnInit {

  categorySelected: any;
  librarySelected: any;

  constructor(
    public navCtrl: NavController,
    public events: Events,
    public route: ActivatedRoute,
    public globalService: GlobalService
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      console.log(params);
      
      if (params['filter']) {
        this.categorySelected = JSON.parse(params['filter']).categorySelected;
        this.librarySelected = JSON.parse(params['filter']).librarySelected;
      }
    });

    this.events.subscribe("category:data", (data) => {
      this.categorySelected = data;
    });

    this.events.subscribe("library:data", (data) => {
      this.librarySelected = data;
    })
  }

  ngOnDestroy(): void {
    this.events.unsubscribe("category:data");
    this.events.unsubscribe("library:data");
  }

  filterCategory() {
    this.navCtrl.navigateForward("/categories");
  }

  filterLibrary() {
    this.navCtrl.navigateForward("/filter-library-list");
  }

  resetFilter() {
    this.categorySelected = null;
    this.librarySelected = null;
    this.globalService.filterBook.next("");
    this.navCtrl.back();
  }

  applyFilter() {
    let data = {
      categorySelected: (this.categorySelected) ? this.categorySelected : "",
      librarySelected: (this.librarySelected) ? this.librarySelected : "",
    }

    this.globalService.filterBook.next(data);
    this.navCtrl.back();
  }

  back(){
    this.navCtrl.back();
  }
}
