import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { RestApiService } from 'src/app/services/rest-api.service';
import { NavController, IonInput, Events, ActionSheetController } from '@ionic/angular';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { NavService } from 'src/app/services/nav.service';
import { ModalController } from '@ionic/angular';
import { FilterBookPage } from './filter-book/filter-book.page';
import { StoragePathService } from 'src/app/services/storage-path.service';
import { GlobalService } from 'src/app/services/global.service';
@Component({
  selector: 'app-book',
  templateUrl: './book.page.html',
  styleUrls: ['./book.page.scss'],
})
export class BookPage implements OnInit {

  @ViewChild('searchInput') searchInput: IonInput;

  isLoading: boolean;
  loading: any;
  bookList: any[] = [];
  keyword = new FormControl();
  formData = {
    page: 1,
    limit: 6,
    category: [],
    filter: '',
    keyword: '',
    sortBy: '',
    filterFromHome: '',
  }


  page: any = 1;
  coverPath: any;
  totalPage = 1;
  titleList: Array<string> = [];
  isClickTitle: boolean;

  constructor(
    public restApi: RestApiService,
    public route: ActivatedRoute,
    public navCtrl: NavController,
    public navService: NavService,
    public events: Events,
    public modalController: ModalController,
    public storagePath: StoragePathService,
    public globalService: GlobalService,
    public actionSheetController: ActionSheetController
  ) { }

  ngOnInit() {
    this.runEvents();

    this.route.queryParams.subscribe(params => {
      this.formData.filterFromHome = params['filterFromHome'];
      if (this.formData.filterFromHome != '' && this.formData.filterFromHome != undefined) {
        this.getBookList();
      }

      if (!this.formData.filterFromHome) {
        setTimeout(() => {
          this.searchInput.setFocus();
        }, 400);
      }
    });

  }

  runEvents() {
    this.globalService.filterBook.subscribe((data: any) => {
      this.formData.page = 1;
      this.formData.filter = data;
      this.getBookList();
    });

    this.globalService.sorting.subscribe((data: any) => {
      this.formData.page = 1;
      this.formData.filterFromHome = data;
      this.getBookList();
    });
  }

  async getBookList() {
    this.isLoading = true;
    this.restApi.getBookList(this.formData).subscribe(async (results: any) => {
      this.bookList = results.data;
      this.totalPage = results.totalPage;
      this.isLoading = false;
      this.page = results.page;
    },
      err => {
        this.isLoading = false;
      })
  }

  searchTitle(e) {
    this.isLoading = true;
    if (e.target.value != '') {
      this.restApi.searchTitleBook(e.target.value)
        .subscribe(result => {
          this.titleList = result.data;
          this.page = result.page;
          this.totalPage = result.totalPage;
          this.isLoading = false;
        }, error => {
          this.isLoading = false;
        })
    }
    else {
      this.titleList = [];
      this.formData.keyword = '';
      this.isLoading = false;
      this.getBookList();
    }
  }

  searchBook(title) {
    this.isClickTitle = true;
    this.formData.keyword = title;
    this.formData.page = 1;
    this.titleList = [];
    this.bookList = [];
    this.isLoading = true;

    this.restApi.getBookList(this.formData).subscribe((results: any) => {
      this.bookList = results.data;
      this.totalPage = results.totalPage;
      this.page = results.page;
      this.formData.page = this.page;
      this.isLoading = false;
    },
      error => {
        this.isClickTitle = false;
        this.isLoading = false;
      });
  }

  doInfinite(event, page) {
    this.formData.page = page + 1;
    this.restApi.getBookList(this.formData).subscribe((results: any) => {
      this.totalPage = results.totalPage;
      this.page = results.page;
      this.formData.page = this.page;

      if (results.data.length > 0) {
        for (let item of results.data) {
          this.bookList.push(item);
        }
      }

      event.target.complete();
    }, err => {
      event.target.complete();
    })
  }

  gotoDetail(bookID, book) {
    this.navCtrl.navigateForward(['book-detail'], { queryParams: { bookID: bookID, book: book } });
  }

  filter() {
    this.navCtrl.navigateForward(['/filter-book'], { queryParams: { filter: JSON.stringify(this.formData.filter) } });
  }

  sorting() {
    this.navCtrl.navigateForward(['/book-sorting'], { queryParams: { sortBy: this.formData.filterFromHome } });
  }

  back() {
    this.navCtrl.back();
  }
}
