import { Component, OnInit, ViewChild } from '@angular/core';
import { RestApiService } from 'src/app/services/rest-api.service';
import { GlobalService } from 'src/app/services/global.service';
import { IonInput, NavController } from '@ionic/angular';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.page.html',
  styleUrls: ['./book-list.page.scss'],
})
export class BookListPage implements OnInit {

  @ViewChild('search') search: IonInput;

  bookList: Array<any> = [];
  libraryID: any;
  staff: any;
  totalPage: any = 0;
  page: any = 1;
  formData = {
    page: 1,
    limit: 6,
    category: [],
    filter: null,
    keyword: '',
    sortBy: '',
    filterFromHome: '',
  }

  isSearchInput: boolean;

  constructor(
    public restApi: RestApiService,
    public globalService: GlobalService,
    public navCtrl: NavController
  ) { }

  ngOnInit() {
    this.staff = JSON.parse(localStorage.getItem('user'));
    if (this.staff) {
      this.libraryID = this.staff.libraryID;
      this.getBookList();
    }
  }

  getBookList() {
    const filter = {
      librarySelected: { libraryID: this.libraryID },
      categorySelected: {}
    }

    this.formData.filter = filter;

    this.restApi.getBookList(this.formData).subscribe((result) => {
      this.bookList = result.data;
    })
  }

  toggleSearch() {
    this.isSearchInput = !this.isSearchInput;
    if (this.isSearchInput) {
      setTimeout(() => {
        this.search.setFocus();
      }, 200)
    }
  }

  searchBook(data) {
    this.formData.keyword = data.detail.value;
    this.formData.page = 1;

    this.restApi.getBookList(this.formData).subscribe((results: any) => {
      this.bookList = results.data;
      this.totalPage = results.totalPage;
      this.page = results.page;
      this.formData.page = this.page;
    },
      err => {
        console.log('error', err);
      });
  }

  doInfinite(event, page) {
    this.formData.page = page + 1;
    this.restApi.getBookList(this.formData).subscribe((result) => {
      this.totalPage = result.totalPage;
      this.page = result.page;
      this.formData.page = this.page;

      if (result.data.length > 0) {
        for (let item of result.data) {
          this.bookList.push(item);
        }
      }

      event.target.complete();
    }, err => {
      event.target.complete();
    })
  }

  gotoDetail(id) {
    this.navCtrl.navigateForward(['/book-detail'], { queryParams: { bookID: id } })
  }
}
