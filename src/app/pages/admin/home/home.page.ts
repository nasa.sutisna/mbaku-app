import { Component, OnInit, ViewChild } from '@angular/core';
import { NavService } from '../../../services/nav.service';
import { RestApiService } from '../../../services/rest-api.service';
import { AlertController, LoadingController, IonInfiniteScroll, NavController, Platform } from '@ionic/angular';
import { FormControl } from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderOptions, NativeGeocoderResult } from '@ionic-native/native-geocoder/ngx';
import { StoragePathService } from 'src/app/services/storage-path.service';
import { GlobalService } from 'src/app/services/global.service';

declare const google;

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  segment:string = 'today';
  user:any;
  content:any;

  constructor(
    public navService: NavService,
    public navCtrl: NavController,
    public restApi: RestApiService,
    public alertCtrl: AlertController,
    public loadingController: LoadingController,
    public geolocation: Geolocation,
    public nativeGeocoder: NativeGeocoder,
    public platform: Platform,
    public storagePath: StoragePathService,
    public globalService: GlobalService
  ) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.getData();
  }

  getData(){
    this.restApi.getDataDashboardAdmin(this.user.libraryID).subscribe((result) => {
      this.content = result;
    })
  }
}


