import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInput, IonSearchbar, NavController } from '@ionic/angular';
import { RestApiService } from 'src/app/services/rest-api.service';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-ebook-list',
  templateUrl: './ebook-list.page.html',
  styleUrls: ['./ebook-list.page.scss'],
})
export class EbookListPage implements OnInit {

  @ViewChild('search') search;

  ebookList:Array<any> = [];
  libraryID:any;
  staff:any;
  totalPage:any;
  page:number = 1;

  formData = {
    page: 1,
    limit: 20,
    category: [],
    filter: null,
    keyword: '',
    sortBy: '',
    filterFromHome: '',
  }

  isSearchInput:boolean;
  constructor(
    public restApi: RestApiService,
    public globalService: GlobalService,
    public navCtrl: NavController
  ) { }

  ngOnInit() {
    this.staff = JSON.parse(localStorage.getItem('user'));
    if(this.staff){
      this.libraryID = this.staff.libraryID;
      this.getEbookList();
    }
  }

  getEbookList(){
    const filter = {
      librarySelected: {libraryID: this.libraryID},
      categorySelected: {}
    }

    this.formData.filter = filter;

    this.restApi.getEbookList(this.formData).subscribe((result) => {
      this.ebookList = result.data;
    })
  }
  
  toggleSearch(){
    this.isSearchInput = !this.isSearchInput;
    if(this.isSearchInput){
      setTimeout(() => {
        this.search.setFocus();
      },200)
    }
  }

  searchEbook(data) {
    this.formData.keyword = data.detail.value;
    this.formData.page = 1;
    
    this.restApi.getEbookList(this.formData).subscribe((results: any) => {
      this.ebookList = results.data;
      this.totalPage = results.totalPage;
      this.page = results.page;
      this.formData.page = this.page;
    },
      err => {
        console.log('error', err);
      });
  }
  
  doInfinite(event, page) {
    this.formData.page = page + 1;
    this.restApi.getEbookList(this.formData).subscribe((result) => {
      this.totalPage = result.totalPage;
      this.page = result.page;
      this.formData.page = this.page;

      if (result.data.length > 0) {
        for (let item of result.data) {
          this.ebookList.push(item);
        }
      }

      event.target.complete();
    }, err => {
      event.target.complete();
    })
  }

  gotoDetail(id) {
    this.navCtrl.navigateForward(['/ebook-detail'], { queryParams: { ebookID: id, role: 1 } })
  }
  

}
