import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EbookListPage } from './ebook-list.page';

describe('EbookListPage', () => {
  let component: EbookListPage;
  let fixture: ComponentFixture<EbookListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EbookListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EbookListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
