import { Component, OnInit } from '@angular/core';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { ActivatedRoute } from '@angular/router';
import { RestApiService } from 'src/app/services/rest-api.service';
import { NavController } from '@ionic/angular';
import { GlobalService } from 'src/app/services/global.service';
import { StoragePathService } from 'src/app/services/storage-path.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.page.html',
  styleUrls: ['./transactions.page.scss'],
})
export class TransactionsPage implements OnInit {

  isOn: boolean = false;
  type: string;
  bookList: Array<any> = [];
  member: any;
  staff: any;
  dueDate: any;
  photoPath: any;
  overDueFee: any;
  overdueDay: any;
  paymentType: string;
  transactionLoanID: Array<any> = [];
  year: any;
  isLoading: boolean;

  constructor(
    private barcodeScanner: BarcodeScanner,
    public route: ActivatedRoute,
    public restApi: RestApiService,
    public navCtrl: NavController,
    public globalService: GlobalService,
    public storagePath: StoragePathService,
  ) {
    const date = new Date().toISOString();
    this.dueDate = date.split('T')[0];
    this.year = new Date().getFullYear();
  }

  ngOnInit() {
    this.staff = JSON.parse(localStorage.getItem('user'));

    this.route.queryParams.subscribe((params) => {
      this.type = params.type
      this.scanMember();
    });
  }

  scanMember() {
    this.barcodeScanner.scan().then(barcodeData => {
      if (barcodeData.cancelled) {
        this.globalService.showToast('Transaksi Dibatalkan')
        setTimeout(() => {
          this.navCtrl.navigateRoot('/home');
        },1500)
        return false;
      }

      this.getMember(barcodeData.text);

    }).catch(err => {
      console.log('Error', err);
    });
  }

  getMember(id) {
    this.isLoading = true;
    this.restApi.getDetailMember(id).subscribe((result: any) => {
      if (result.memberID) {
        this.member = result;
        this.photoPath = this.storagePath.storageProfile(this.member.memberID);

        if (this.type == 'loan-return') {
          this.getMemberBookLoan();
        }

      }
      else {
        this.globalService.showToast('Anggota Tidak Ditemukan');
        this.navCtrl.navigateRoot('home');
      }

      this.isLoading = false;
    }, error => {
      this.globalService.showToast('Anggota Tidak Ditemukan');
      this.navCtrl.navigateRoot('home');
      this.isLoading = false;
    })
  }

  scanBook() {
    this.barcodeScanner.scan().then(barcodeData => {
      if (this.type == 'loan') {
        this.getBook(barcodeData.text)
      }
    }).catch(err => {
      console.log('Error', err);
    });
  }

  getBook(bookID) {
    this.globalService.presentLoading();
    this.restApi.loanGetBook(this.staff.libraryID, bookID).subscribe((result: any) => {
      this.globalService.dismissLoading();
      if (result.msg == "") {
        this.bookList.push(result.data);
      }
      else {
        this.globalService.showToast(result.msg);
      }
    }, error => {
      this.globalService.dismissLoading();
      this.globalService.showToast('Buku tidak ditemukan');
    })
  }

  removeBook(id) {
    this.bookList.splice(this.bookList.findIndex(item => item.bookID == id), 1);
  }

  getMemberBookLoan() {
    this.restApi.getMemberBookLoan(this.staff.libraryID, this.member.memberID)
      .subscribe((result) => {
        this.bookList = result.book
        this.overDueFee = result.overDueFee
        this.overdueDay = result.overdueDay
      })
  }

  saveLoan() {
    const params = {
      libraryID: this.staff.libraryID || null,
      bookID: this.bookList || [],
      memberID: this.member.memberID || 0,
      transactionLoanDueDate: this.dueDate || null
    }

    this.restApi.loanTransaction(params)
      .subscribe((result) => {
        this.globalService.showToast('Transaksi Berhasil Disimpan');
        this.navCtrl.navigateRoot('home')
      }, error => {
        if (error.status == 422) {
          this.globalService.showToast(error.error.msg);
        }
        else if (error.status == 400) {
          this.globalService.showToast(error.error.msg);
        }
        else {
          this.globalService.showToast('Transaksi Gagal Disimpan');
        }
      })
  }

  saveLoanReturn() {
    const params = {
      paymentType: 'cash',
      transactionLoanID: this.bookList,
      bookID: this.bookList,
      overDueFee: this.overDueFee,
      transactionLoanDueDate: this.dueDate,
      memberID: this.member.memberID
    }

    this.globalService.presentLoading();
    this.restApi.returnTransaction(params)
      .subscribe((result) => {
        this.globalService.showToast('Transaksi Berhasil Disimpan');
        this.globalService.dismissLoading();
        this.navCtrl.navigateRoot('home')
      }, error => {
        console.log(error);
        this.globalService.dismissLoading();
        if (error.status == 422) {
          this.globalService.showToast(error.error.message);
        }
        else {
          this.globalService.showToast('Transaksi Gagal Disimpan');
        }
      })
  }

}
