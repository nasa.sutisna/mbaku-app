import { Component, OnInit } from '@angular/core';
import { RestApiService } from 'src/app/services/rest-api.service';
import { GlobalService } from 'src/app/services/global.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ISetting, ISettingValue } from 'src/app/interface/interface';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.page.html',
  styleUrls: ['./setting.page.scss'],
})
export class SettingPage implements OnInit {

  user: any;
  setting: ISetting;
  settingValue: ISettingValue;

  constructor(
    public restApi: RestApiService,
    public globalService: GlobalService
  ) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
    if (this.user) {
      this.getSetting(this.user.libraryID);
    }
  }

  getSetting(id) {
    this.restApi.getSetting(id).subscribe((result) => {
      this.setting = result.librarySetting
      this.settingValue = this.setting.settingValue
    })
  }

  saveSetting() {
    const params = {
      settingID: this.setting.settingID,
      settingValue: JSON.stringify(this.settingValue)
    }

    this.restApi.updateSetting(params).subscribe((result) => {
      this.globalService.showToast(result.message);
    }, (error: HttpErrorResponse) => {
      this.globalService.showToast(error.error.message);
    })
  }

}
