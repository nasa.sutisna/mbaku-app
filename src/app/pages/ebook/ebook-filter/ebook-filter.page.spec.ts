import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EbookFilterPage } from './ebook-filter.page';

describe('EbookFilterPage', () => {
  let component: EbookFilterPage;
  let fixture: ComponentFixture<EbookFilterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EbookFilterPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EbookFilterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
