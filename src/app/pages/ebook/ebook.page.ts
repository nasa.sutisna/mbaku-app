import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { RestApiService } from 'src/app/services/rest-api.service';
import { NavController, IonInput, Events } from '@ionic/angular';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { NavService } from 'src/app/services/nav.service';
import { ModalController } from '@ionic/angular';
import { FilterBookPage } from '../book/filter-book/filter-book.page';
import { StoragePathService } from 'src/app/services/storage-path.service';
import { GlobalService } from 'src/app/services/global.service';
@Component({
  selector: 'app-ebook',
  templateUrl: './ebook.page.html',
  styleUrls: ['./ebook.page.scss'],
})
export class EbookPage implements OnInit {

  @ViewChild('searchInput') searchInput: IonInput;

  isLoading: boolean = true;
  loading: any;
  ebookList: any[] = [];
  keyword = new FormControl();
  formData = {
    page: 1,
    limit: 6,
    category: [],
    filter: '',
    keyword: '',
    sortBy: '',
    filterFromHome: '',
  }

  isClickTitle: boolean = false;

  page: any = 1;
  coverPath: any;
  totalPage = 1;
  selectedSortBy: any = '';
  titleList: Array<string> = [];

  constructor(
    public restApi: RestApiService,
    public route: ActivatedRoute,
    public navCtrl: NavController,
    public navService: NavService,
    public events: Events,
    public modalController: ModalController,
    public storagePath: StoragePathService,
    public globalService: GlobalService
  ) { }

  ngOnInit() {
    this.runEvents();
    this.getEbookList();
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.events.unsubscribe('UPDATE:EBOOKLIST');
  }

  runEvents() {
    this.globalService.filterEbook.subscribe((data: any) => {
      this.formData.page = 1;
      this.formData.filter = data;
      this.getEbookList();
    });

    this.events.subscribe('UPDATE:EBOOKLIST', () => {
      this.getEbookList();
    })
  }

  async getEbookList() {
    this.isLoading = true;
    this.restApi.getEbookList(this.formData).subscribe(async (results: any) => {
      this.ebookList = results.data;
      this.totalPage = results.totalPage;
      this.page = results.page;
      this.isLoading = false;
    },
      err => {
        this.isLoading = false;
      })
  }

  searchTitle(e) {
    this.isLoading = true;
    if (e.target.value != '') {
      this.restApi.searchTitleEbook(e.target.value)
        .subscribe(result => {
          this.titleList = result.data;
          this.page = result.page;
          this.totalPage = result.totalPage;
          this.isLoading = false;
        })
    }
    else {
      this.isLoading = false;
      this.titleList = [];
      this.searchBook('')
    }
  }

  searchBook(title) {
    this.isClickTitle = true;
    this.formData.keyword = title;
    this.formData.page = 1;
    this.titleList = [];

    this.restApi.getEbookList(this.formData).subscribe((results: any) => {
      this.ebookList = results.data;
      this.totalPage = results.totalPage;
      this.page = results.page;
      this.formData.page = this.page;
      this.isLoading = false;
    },
      err => {
        console.log('error', err);
      });
  }

  doInfinite(event, page) {
    this.formData.page = page + 1;
    this.restApi.getEbookList(this.formData).subscribe((results: any) => {
      this.totalPage = results.totalPage;
      this.page = results.page;
      this.formData.page = this.page;

      if (results.data.length > 0) {
        for (let item of results.data) {
          this.ebookList.push(item);
        }
      }

      event.target.complete();
    }, err => {
      event.target.complete();
    })
  }

  gotoDetail(ebookID, ebook) {
    this.navCtrl.navigateForward(["ebook-detail"], { queryParams: { ebookID: ebookID, ebook: ebook } });
  }

  sortBy(item) {
    this.selectedSortBy = item;
    this.formData.filterFromHome = item;
    this.formData.page = 1;
    this.getEbookList();
  }

  filter() {
    this.navCtrl.navigateForward(['/ebook-filter'], { queryParams: { filter: JSON.stringify(this.formData.filter) } });
  }
}
