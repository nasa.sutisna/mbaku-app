import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EbookViewerPage } from './ebook-viewer.page';

describe('EbookViewerPage', () => {
  let component: EbookViewerPage;
  let fixture: ComponentFixture<EbookViewerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EbookViewerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EbookViewerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
