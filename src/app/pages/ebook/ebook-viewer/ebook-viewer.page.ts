import { Component, OnInit } from '@angular/core';
import { NavService } from './../../../services/nav.service';
import { RestApiService } from './../../../services/rest-api.service';
import { GlobalService } from './../../../services/global.service';
import { NavController, AlertController } from '@ionic/angular';
import { DatePipe } from '@angular/common';
declare var snap: any;
@Component({
  selector: 'app-ebook-viewer',
  templateUrl: './ebook-viewer.page.html',
  styleUrls: ['./ebook-viewer.page.scss'],
})
export class EbookViewerPage implements OnInit {

  ebookID: any;
  title: any;
  ebook: any;
  totalPage: any = 0;
  ratio: any = 0.4;
  ratioPercentage: any = 20;
  isLoading: any;
  book: any;
  bookId: any;
  intervalCheckStatus: any;
  myRate: any = 0;
  isPendingPayment: boolean = false;
  isFirst: boolean = true;
  isSettlement: boolean = false;
  expireDate:any;

  statusOrder: any = {};
  price: any = 0;
  itemBook: any;
  paramsOrder = {
    full_name: '',
    email: '',
    phone: '',
    kode_buku: '',
    judul: '',
    price: ''
  }

  ebookPrice:any;

  constructor(
    public navService: NavService,
    public restApi: RestApiService,
    public globalService: GlobalService,
    public navCtrl: NavController,
    public alertController: AlertController,
    private datePipe: DatePipe
  ) { }

  ngOnInit() {
    this.ebookID = this.navService.get('ebookID');
    this.itemBook = this.navService.get('ebook');
    this.ebookPrice = Number(this.itemBook.ebookPrice);
    this.expireDate = this.datePipe.transform(this.navService.get('expireDate'),'dd-MM-yyyy');
  }

  ngOnDestroy(): void {
    clearInterval(this.intervalCheckStatus)
  }

  handlePageEbook(e) {
    this.totalPage = e;
  }

  handleLoading(e) {
    this.isLoading = e;
  }

  handleError(e){
    if(e){
      this.navCtrl.back();
      this.globalService.showToast('File Ebook Tidak Ditemukan');
    }
  }

  countRatio(action = '') {
    this.ratio = this.ratio * 10;
    if (action == 'plus') {
      if (this.ratio < 10) {
        this.ratio++;
      }
    }
    else {
      if (this.ratio > 1) {
        this.ratio--;
      }
    }

    this.ratioPercentage = this.ratio * 10;
    this.ratio = this.ratio / 10;
  }


  async presentAlert() {
    const alert = await this.alertController.create({
      mode: 'ios',
      header: `INFORMASI`,
      message: 'Ebook ini dapat dibaca sampai tanggal ' + this.expireDate,
      buttons: ['OK']
    });

    await alert.present();
  }

  download() {
    this.globalService.presentLoading('Mengunduh...');
    this.globalService.download(this.restApi.apiUrlStorage + 'ebook/' + this.itemBook.libraryID +'/'+ this.itemBook.ebookFile, this.itemBook.ebookFile).then((result) => {
      console.log('download successs',result);
      this.globalService.dismissLoading();
    }).catch((error) => {
      this.globalService.dismissLoading();
      console.log('download error',error);
    })
  }

  back() {
    this.navCtrl.back();
  }
}
