import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EbookViewerPage } from './ebook-viewer.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DatePipe } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: EbookViewerPage
  }
];

@NgModule({
  providers : [
    DatePipe
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    FlexLayoutModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EbookViewerPage],
})
export class EbookViewerPageModule {}
