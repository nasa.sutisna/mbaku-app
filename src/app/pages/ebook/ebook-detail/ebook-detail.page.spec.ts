import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EbookDetailPage } from './ebook-detail.page';

describe('EbookDetailPage', () => {
  let component: EbookDetailPage;
  let fixture: ComponentFixture<EbookDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EbookDetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EbookDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
