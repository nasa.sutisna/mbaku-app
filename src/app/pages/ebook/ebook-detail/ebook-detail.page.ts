import { Component, OnInit, ViewChild } from '@angular/core';
import { NavService } from 'src/app/services/nav.service';
import { RestApiService } from 'src/app/services/rest-api.service';
import { AuthService } from 'src/app/services/auth.service';
import { NavController, ActionSheetController, IonContent, Events, Platform } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { StoragePathService } from 'src/app/services/storage-path.service';
import { GlobalService } from 'src/app/services/global.service';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { AppAvailability } from '@ionic-native/app-availability/ngx';
import { ActionSheetOptions } from '@ionic/core';
declare const snap: any;

export interface IMidtrans {
  redirect_url: string;
  token: string
}

@Component({
  selector: 'app-ebook-detail',
  templateUrl: './ebook-detail.page.html',
  styleUrls: ['./ebook-detail.page.scss'],
})
export class EbookDetailPage implements OnInit {
  @ViewChild(IonContent) content: IonContent;
  ebookItem: any;
  ebookID: any;
  ebookList: any = [];
  segment1: any = 'description';

  formData = {
    page: 1,
    limit: 6,
    category: [],
    filter: null,
    keyword: '',
    sortBy: '',
    filterFromHome: '',
  }

  paramsOrder = {
    full_name: '',
    email: '',
    phone: '',
    ebookID: '',
    ebookTitle: '',
    ebookPrice: '',
    paymentType: ''
  }

  memberStatus = {
    memberRole: null,
    memberSaldo: null,
  }

  feed: any;
  userData: any;
  allowedRead: boolean;
  showFooter: boolean;
  role: any = 0;
  gojekExist: boolean = false;
  expireDate: any;
  intervalCheck: any;
  paymentPending: boolean = false;
  isOpenGojek: boolean = false;
  gojekToken: any;
  isLoading: boolean = false;
  isLoadingList: boolean = false;

  constructor(
    public navService: NavService,
    public restApi: RestApiService,
    public authService: AuthService,
    public navCtrl: NavController,
    public route: ActivatedRoute,
    public storagePath: StoragePathService,
    public globalService: GlobalService,
    public events: Events,
    public inAppBrowser: InAppBrowser,
    public platform: Platform,
    public appAvailability: AppAvailability,
    public actionSheetController: ActionSheetController,
  ) {

    this.platform.pause.subscribe((data) => {

    });

    this.platform.resume.subscribe((data) => {
      if (this.isOpenGojek) {
        this.getPaymentStatus(this.gojekToken);
      }
    });

  }

  ngOnInit() {
    this.userData = JSON.parse(localStorage.getItem('user'));
    this.route.queryParams.subscribe((params) => {
      this.ebookID = params['ebookID'];
      this.role = params['role'];
      this.checkMyFeedBack();
      this.getDetailEbook(this.ebookID);
      this.checkAppAvailability();
      this.checkStatusPending();
    });
  }

  ngOnDestroy(): void {
    clearInterval(this.intervalCheck);
    this.isOpenGojek = false;
  }

  getPaymentStatus(token) {
    const params = {
      id: token
    }

    this.restApi.getStatusPayment(params).subscribe((result: any) => {
      if (result.transaction_status == 'settlement') {
        this.savePayment(result.token, result);
      }
    })
  }

  checkStatusPending() {
    const params = {
      memberID: this.userData.memberID
    }

    this.restApi.checkPendingPaymentEbook(params).subscribe((result) => {
      this.paymentPending = result.paymentPending;
      if (result.paymentPending) {
        this.intervalCheck = setInterval(() => {
          this.checkPaymentPending(result.paymentToken);
        }, 5000)
      }
    })
  }

  checkAppAvailability() {
    if (this.platform.is('cordova')) {
      const app = 'com.gojek.app';
      this.appAvailability.check(app)
        .then((result) => {
          this.gojekExist = result;
          console.log(result);
        });
    }
  }

  getDetailEbook(ebookID) {
    this.isLoading = true;
    this.isLoadingList = true;

    this.restApi.getDetailEbook(ebookID).subscribe((result) => {
      this.ebookItem = result;
      this.checkMemberStatus();
      this.checkAccess();
      this.getBookList();
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
      this.isLoadingList = false;
    })
  }

  checkMemberStatus() {
    this.restApi.checkMemberStatus({ memberID: this.userData.memberID }).subscribe((result) => {
      this.memberStatus = result;
    })
  }

  async getBookList() {
    this.formData.filter = {
      librarySelected: { libraryID: this.ebookItem.libraryID },
      categorySelected: { categoryID: this.ebookItem.categoryID }
    }

    this.restApi.getEbookList(this.formData).subscribe(async (results: any) => {
      this.ebookList = results.data;
      this.isLoadingList = false;
    },
      err => {
        this.isLoadingList = false;
      })
  }

  gotoEbook() {
    this.checkAccess();
    if (this.authService.isLogin) {
      this.navService.push("ebook", { ebookID: this.ebookItem.ebookID, ebook: this.ebookItem, expireDate: this.expireDate });
    }
    else {
      this.navCtrl.navigateForward("login");
    }
  }

  detail(ebookItem) {
    this.globalService.presentLoading();
    this.ebookItem = ebookItem;
    this.checkAccess();
    setTimeout(() => {
      this.ScrollToTop();
      this.globalService.dismissLoading();
    }, 500)
  }

  async presentActionSheet() {

    let options: ActionSheetOptions = {
      header: 'Metode Pembayaran',
      buttons: []
    }

    this.checkAppAvailability();

    if (this.memberStatus.memberRole && this.memberStatus.memberSaldo >= this.ebookItem.ebookPrice) {
      options.buttons.push({
        text: 'Saldo MBAKU',
        icon: 'wallet',
        handler: () => {

          const time = new Date().getTime();
          const params = {
            order_id: 'MBAKU-' + time,
            payment_type: 'saldo',
            gross_amount: this.ebookItem.ebookPrice,
            transaction_status: 'settlement',
            saldo: this.memberStatus.memberSaldo
          }

          this.savePayment(null, params);
        }
      })
    }

    options.buttons.push(
      {
        text: 'GOPAY',
        icon: 'radio-button-on',
        handler: () => {
          if (this.gojekExist) {
            this.openSnap('gopay');
          }
          else {
            this.globalService.showToast('Aplikasi Gojek tidak ditemukan di perangkat kamu');
          }
        }
      }, {
        text: 'ATM/Bank Transfer',
        icon: 'card',
        handler: () => {
          this.openSnap('bank_transfer');
        }
      },
      {
        text: 'Batalkan',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }
    )

    const actionSheet = await this.actionSheetController.create(options);
    await actionSheet.present();
  }

  openSnap(paymentType) {

    if (this.paymentPending) {
      this.globalService.showToast('Kamu masih memiliki pembayaran yang belum dilunasi', 'bottom', 5000);
      return false;
    }

    let auth: any = JSON.parse(localStorage.getItem('user'));

    this.paramsOrder.full_name = auth.memberFirstName + ' ' + auth.memberLastName;
    this.paramsOrder.email = auth.memberEmail;
    this.paramsOrder.phone = auth.memberPhone;
    this.paramsOrder.ebookID = this.ebookItem.ebookID;
    this.paramsOrder.ebookTitle = this.ebookItem.ebookTitle;
    this.paramsOrder.ebookPrice = this.ebookItem.ebookPrice;
    this.paramsOrder.paymentType = paymentType;

    this.globalService.presentLoading();

    this.restApi.orderEbook(this.paramsOrder).subscribe(async (result: IMidtrans) => {

      if (paymentType == 'bank_transfer') {
        this.bankTransfer(result.token)
      }
      else {
        this.gopay(result.token, result.redirect_url);
      }

      this.globalService.dismissLoading();
    }, error => {
      this.globalService.dismissLoading();
    });

  }

  bankTransfer(token) {
    snap.pay(token, {
      enabledPayments: ["bca_va", "permata_va", "bni_va", "echannel", "other_va"],

      onSuccess: (response) => {
        this.savePayment(token, response);
        console.log('sukses', response);
      },
      onPending: (response) => {
        console.log('sukses 2', response);
        if (response.payment_type != 'gopay') {
          this.savePayment(token, response);
          this.checkPaymentPending(token);
        }
      },
      onError: (response) => {
        console.log(response);
      },
      onClose: () => {
        console.log('customer closed the popup without finishing the payment');
      }
    });
  }

  gopay(token, redirect_url) {

    const options: InAppBrowserOptions = {
      location: 'no',
      hidenavigationbuttons: 'yes',
      toolbar: 'no',
      hardwareback: 'yes',
      fullscreen: 'yes'
    }

    const browser = this.inAppBrowser.create(redirect_url, '_blank', options)

    if (this.platform.is('cordova')) {
      browser.on("loadstop")
        .subscribe(response => {
          console.log('response iab', response);
          let loopDeeplink = setInterval(() => {
            let url = response.url;
            if (url.indexOf('gojek://gopay') > -1) {
              browser.close();
              clearInterval(loopDeeplink);
              if (this.gojekExist) {
                this.openDeeplink(url);
                this.isOpenGojek = true;
                this.gojekToken = token;
              }
              else {
                this.globalService.showToast('Aplikasi Gojek tidak ditemukan di perangkat kamu');
              }
            }

          }, 200);
        });
    }
  }

  openDeeplink(deeplink) {
    const browser = this.inAppBrowser.create(deeplink, '_system', 'location=no');
    browser.on("loadstop")
      .subscribe(response => {
        console.log('response iab', response);
        let loopDeeplink = setInterval(() => {
          console.log('response iab', response);
        }, 500);
      });
  }

  checkPaymentPending(token) {
    const params = {
      id: token
    }

    this.restApi.getStatusPayment(params).subscribe((result: any) => {
      if (result.transaction_status != 'pending') {
        this.updatePaymentEbook(result);
      }
    })
  }

  updatePaymentEbook(params) {
    if (params.transaction_status == 'expire') {
      params.payment_token = params.token
    }

    params.ebookID = this.ebookID;
    params.libraryID = this.ebookItem.libraryID
    params.memberID = this.userData.memberID

    this.restApi.updatePaymentEbook(params).subscribe((result) => {
      this.getDetailEbook(this.ebookID);
    });
  }

  savePayment(token, paymentResult) {
    this.globalService.presentLoading();

    try {
      let params = {
        ebookID: this.ebookItem.ebookID,
        memberID: this.userData.memberID,
        libraryID: this.ebookItem.libraryID,
        payment_token: token ? token : null
      }

      params = Object.assign(params, paymentResult);
      this.restApi.saveEbookPayment(params)
        .subscribe((result) => {

          if(paymentResult.transaction_status == 'settlement'){
            this.gotoEbook();
          }

          this.events.publish('MEMBER:UPDATEPROFILE',true);
          this.getDetailEbook(this.ebookID);
          this.globalService.dismissLoading();
        }, error => {
          this.globalService.dismissLoading();
        })
    }
    catch (error) {
      console.log(error);
      this.globalService.dismissLoading();
    }
  }

  checkMyFeedBack() {
    const params = {
      memberID: this.userData.memberID,
      ebookID: this.ebookID
    }

    this.restApi.checkMyFeedBack(params).subscribe((result: any) => {
      this.feed = result.feed;
    })
  }

  checkAccess() {
    let params = {
      ebookID: this.ebookItem.ebookID,
      memberID: this.userData.memberID
    }

    this.restApi.checkAccessRead(params).subscribe((result) => {
      this.allowedRead = result.allowedRead
      this.expireDate = result.expireDate
      this.showFooter = true;
    }, error => {
      this.showFooter = false;
    })
  }

  ScrollToTop() {
    this.content.scrollToTop(1000);
  }

  addFeedBack() {
    this.feed = !this.feed;

    const params = {
      memberID: this.userData.memberID,
      ebookID: this.ebookID,
      feedBackValue: this.feed ? 1 : 0,
    }

    this.ebookItem.feedBack = params.feedBackValue == 0 ? parseInt(this.ebookItem.feedBack) - 1 : parseInt(this.ebookItem.feedBack) + 1;

    this.restApi.addFeedBack(params).subscribe((result) => {
      this.events.publish('UPDATE:EBOOKLIST')
      this.events.publish('MEMBER:UPDATEPROFILE',true);
    })
  }

  back() {
    this.navCtrl.back();
  }
}
