import { Component, OnInit } from '@angular/core';
import { RestApiService } from 'src/app/services/rest-api.service';
import { NavController, Events } from '@ionic/angular';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.page.html',
  styleUrls: ['./categories.page.scss'],
})
export class CategoriesPage implements OnInit {
  formData = {
    page: 1,
    limit: 20,
    keyword: '',
  }

  totalPage: any = 1;
  page: any = 1;
  categoryList: any = [];

  constructor(
    public restApi: RestApiService,
    public navCtrl: NavController,
    public events: Events
  ) { }

  ngOnInit() {
    this.getCategory();
  }

  getCategory() {
    this.restApi.getCategory(this.formData)
      .subscribe((result) => {
        this.categoryList = result.data;
      });
  }

  doInfinite(event, page) {
    this.formData.page = page + 1;
    this.restApi.getCategory(this.formData)
      .subscribe((results: any) => {
        this.totalPage = results.totalPage;
        this.page = results.page;
        this.formData.page = this.page;

        if (results.data.length > 0) {
          for (let item of results.data) {
            this.categoryList.push(item);
          }
        }

        event.target.complete();
      }, err => {
        event.target.complete();
      })
  }

  dismiss(item) {
    this.events.publish("category:data", item)
    this.navCtrl.back();
  }

  back() {
    this.navCtrl.back();
  }
}
