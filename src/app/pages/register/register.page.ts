import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { ToastController, NavController } from '@ionic/angular';
import { RestApiService } from 'src/app/services/rest-api.service';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  isShowPassword:boolean = false;
  isShowRtypePassword:boolean = false;

  constructor(
    public router: Router,
    public authService: AuthService,
    public restApi: RestApiService,
    public toastCtrl: ToastController,
    public navCtrl: NavController,
    public globalService: GlobalService
  ) { }

  ngOnInit() {
  }

  register(form) {

    if (form.value.password != form.value.retypePassword) {
      this.globalService.showToast('Konfirmasi password tidak cocok');
      return false;
    }
    
    this.restApi.registerAccount(form.value).subscribe((result: any) => {
      this.globalService.showToast(result.msg,'bottom',10000);
      this.navCtrl.pop();
    }, error => {
      this.globalService.showToast(error.error.msg,'bottom',10000);
    });
  }


  backButton() {
    this.navCtrl.pop();
  }

}
