import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterLibraryListPage } from './filter-library-list.page';

describe('FilterLibraryListPage', () => {
  let component: FilterLibraryListPage;
  let fixture: ComponentFixture<FilterLibraryListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterLibraryListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterLibraryListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
