import { Component, OnInit } from '@angular/core';
import { RestApiService } from 'src/app/services/rest-api.service';
import { Events, NavController } from '@ionic/angular';

@Component({
  selector: 'app-filter-library-list',
  templateUrl: './filter-library-list.page.html',
  styleUrls: ['./filter-library-list.page.scss'],
})
export class FilterLibraryListPage implements OnInit {
  formData = {
    page: 1,
    limit: 20,
    keyword: '',
  } 

  totalPage: any = 1;
  page: any = 1;
  libraryList:any = [];

  constructor(
    public restApi: RestApiService,
    public events: Events,
    public navCtrl: NavController
  ) { }

  ngOnInit() {
    this.getListLibrary();
  }

  getListLibrary(){
    this.restApi.getListLibrary(this.formData)
    .subscribe((result) => {
      this.libraryList = result.data;
    })
  }

  doInfinite(event, page) {
    this.formData.page = page + 1;
    this.restApi.getListLibrary(this.formData)
      .subscribe((results: any) => {
        this.totalPage = results.totalPage;
        this.page = results.page;
        this.formData.page = this.page;

        if (results.data.length > 0) {
          for (let item of results.data) {
            this.libraryList.push(item);
          }
        }

        event.target.complete();
      }, err => {
        event.target.complete();
      })
  }


  dismiss(item){
    this.events.publish("library:data",item)
    this.navCtrl.back();
  }

  back() {
    this.navCtrl.back();
  }

}
