import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { RestApiService } from 'src/app/services/rest-api.service';
import { Events, NavController, IonInput, IonInfiniteScroll } from '@ionic/angular';
import { StoragePathService } from 'src/app/services/storage-path.service';
import { FormControl } from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation/ngx';

declare var google;
@Component({
  selector: 'app-library',
  templateUrl: './library.page.html',
  styleUrls: ['./library.page.scss'],
})
export class LibraryPage implements OnInit {
  @ViewChild('searchInput') searchInput: IonInput;

  isLoading: boolean = true;
  loading: any;
  bookList: any[] = [];
  keyword = new FormControl();
  totalPage = 1;
  formData = {
    page: 1,
    limit: 6,
    keyword: '',
  }

  libraryList: any = [];
  libraryPathPhoto: any;

  constructor(
    public restApi: RestApiService,
    public events: Events,
    public storagePath: StoragePathService,
    public navCtrl: NavController,
    public ngZone: NgZone,
    public geolocation: Geolocation
  ) { }

  ngOnInit() {
    this.getListLibrary();
    this.getPathUrl();
  }

  getPathUrl() {
    this.libraryPathPhoto = this.storagePath.storageLibraryPhoto();
  }

  getListLibrary() {
    this.isLoading = true;
    this.restApi.getListLibrary(this.formData)
      .subscribe((result) => {
        this.libraryList = result.data;
        this.getDistance(this.libraryList);
        this.totalPage = result.totalPage;
        this.formData.page = result.page;
        this.isLoading = false;
      }, error => {
        this.isLoading = false;
      })
  }

  getDistance(nearbyList: any = []) {
    this.geolocation.getCurrentPosition().then((position) => {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };

      let getAddress = nearbyList.map((list) => { return list.libraryAddress });
      // let getLatLong = nearbyList.map((list) => { return list.libraryLatLong });

      var origin1 = new google.maps.LatLng(pos);
      var service = new google.maps.DistanceMatrixService();
      service.getDistanceMatrix(
        {
          origins: [origin1],
          destinations: getAddress,
          travelMode: 'DRIVING',

        }, (response, status) => {
          this.ngZone.run(() => {
            for (let key in nearbyList) {
              nearbyList[key]['distance'] = response.rows[0]['elements'][key]['distance']['text'];
              nearbyList[key]['distance_number'] = response.rows[0]['elements'][key]['distance']['value'];
            }

            nearbyList.sort(function (a, b) {
              return parseFloat(a.distance_number) - parseFloat(b.distance_number);
            });
          })
        });
    });
  }

  search(e) {
    this.formData.page = 1;
    this.isLoading = true;
    this.restApi.getListLibrary(this.formData)
      .subscribe((result) => {
        this.isLoading = false;
        this.libraryList = result.data;
        this.getDistance(this.libraryList);
      }, error => {
        this.isLoading = false;
      })
  }

  doInfinite(infiniteScroll) {
    let page = this.formData.page + 1;
    this.formData.page = page;
    this.restApi.getListLibrary(this.formData)
      .subscribe((result) => {
        this.formData.page = result.page;
        if (result.data.length > 0) {
          for (let list of result.data) {
            this.libraryList.push(list);
          }
        }

        infiniteScroll.target.complete();
        this.getDistance(this.libraryList);
      }, error => {
        infiniteScroll.target.complete();
      })
  }

  gotoDetail(list) {
    this.navCtrl.navigateForward(["/library-detail"], { queryParams: { data: JSON.stringify(list) } })
  }

  dismiss(item) {
    this.events.publish("library:data", item)
    this.navCtrl.pop();
  }

  back() {
    this.navCtrl.back();
  }
}
