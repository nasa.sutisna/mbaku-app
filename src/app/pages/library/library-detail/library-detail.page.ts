import { Component, OnInit, ViewChild, ElementRef, AfterContentInit, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestApiService } from 'src/app/services/rest-api.service';
import { StoragePathService } from 'src/app/services/storage-path.service';
import { NavController, ActionSheetController } from '@ionic/angular';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { GlobalService } from 'src/app/services/global.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';

declare var google;

@Component({
  selector: 'app-library-detail',
  templateUrl: './library-detail.page.html',
  styleUrls: ['./library-detail.page.scss'],
})
export class LibraryDetailPage implements OnInit, AfterContentInit {
  map;
  @ViewChild('mapElement') mapElement;

  libItem: any;
  libID: any;
  libraryList: any = [];
  segment1: any = 'detail';
  isLoading: boolean = true;
  isLoadingDetail: boolean = true;

  formDataBook = {
    page: 1,
    limit: 20,
    category: [],
    filter: null,
    keyword: '',
    sortBy: '',
    filterFromHome: '',
  }

  formDataEbook = {
    page: 1,
    limit: 20,
    category: [],
    filter: null,
    keyword: '',
    sortBy: '',
    filterFromHome: '',
  }

  page: any = 1;
  coverPath: any;
  bookList: any = [];
  ebookList: any = [];
  totalPage = 1;
  path: string;
  settingValue: any;

  constructor(
    public route: ActivatedRoute,
    public restApi: RestApiService,
    public storagePath: StoragePathService,
    public navCtrl: NavController,
    public actionSheetController: ActionSheetController,
    private emailComposer: EmailComposer,
    private callNumber: CallNumber,
    public globalService: GlobalService,
    public ngZone: NgZone,
    public geolocation: Geolocation
  ) { }

  ngOnInit() {
    this.path = this.storagePath.storageLibraryPhoto();

    this.route.queryParams.subscribe((params) => {
      let item = JSON.parse(params['data']);
      this.libID = (item) ? item.libraryID : null;

      let filter = {
        librarySelected: { libraryID: this.libID },
        categorySelected: {}
      }
      this.formDataBook.filter = filter;
      this.formDataEbook.filter = filter;
      this.getDetailLibrary();
    })
  }


  ngAfterContentInit(): void {

  }

  getDetailLibrary() {
    this.isLoadingDetail = true;

    this.restApi.getDetailLibrary(this.libID)
      .subscribe((result) => {
        this.libItem = result;
        this.settingValue = JSON.parse(this.libItem.settingValue);
        this.getDistance(this.libItem);
        this.getBookList();
        this.getEbookList();
        this.isLoadingDetail = false;
      }, error => {
        this.isLoadingDetail = false;
      })
  }

  async getBookList() {
    this.isLoading = true;
    this.restApi.getBookList(this.formDataBook).subscribe(async (results: any) => {
      this.bookList = results.data;
      this.totalPage = results.totalPage;
      this.page = results.page;
      this.isLoading = false;
    },
      error => {
        this.isLoading = false;
      })
  }

  async getEbookList() {
    this.isLoading = true;
    this.restApi.getEbookList(this.formDataEbook).subscribe(async (results: any) => {
      this.ebookList = results.data;
      this.totalPage = results.totalPage;
      this.page = results.page;
      this.isLoading = false;
    },
      err => {
        this.isLoading = false;
      })
  }


  gotoDetail(bookID, book) {
    this.navCtrl.navigateForward(["book-detail"], { queryParams: { bookID: bookID, book: book } });
  }

  gotoDetailEbook(ebookID, ebook) {
    this.navCtrl.navigateForward(["ebook-detail"], { queryParams: { ebookID: ebookID, ebook: ebook } });
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Hubungi Perpustakaan',
      buttons: [{
        text: 'Kirim Email',
        icon: 'mail',
        handler: () => {
          this.sendMail();
        }
      }, {
        icon: 'call',
        text: 'Telepon',
        handler: () => {
          this.call();
        }
      },
      {
        text: 'Batal',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });

    actionSheet.buttons.push({})
    await actionSheet.present();
  }

  sendMail() {

    if (!this.libItem.libraryEmail) {
      this.globalService.showToast('Email tidak ditemukan');
      return;
    }

    let email = {
      to: this.libItem.libraryEmail,
      bcc: 'mbakuteam@gmail.com',
      subject: 'Bantuan MBAKU App',
      isHtml: true
    }

    this.emailComposer.open(email);
  }

  call() {
    if (!this.libItem.libraryPhone) {
      this.globalService.showToast('Nomor tidak ditemukan');
      return;
    }

    this.callNumber.callNumber(this.libItem.libraryPhone, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }



  getDistance(item) {
    this.geolocation.getCurrentPosition().then((position) => {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };

      var origin1 = new google.maps.LatLng(pos);
      var service = new google.maps.DistanceMatrixService();
      service.getDistanceMatrix(
        {
          origins: [origin1],
          destinations: [item.libraryAddress],
          travelMode: 'DRIVING',

        }, (response, status) => {
          this.ngZone.run(() => {
            console.log(response);
            item.distance =  response.rows[0]['elements'][0]['distance']['text'];
            item.distance_number = response.rows[0]['elements'][0]['distance']['value'];
          })
        });
    })
      .catch(err => {
        console.log('err', err);
      })
  }

  back() {
    this.navCtrl.back();
  }

}
