import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, IonSlide, IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding.page.html',
  styleUrls: ['./onboarding.page.scss'],
})
export class OnboardingPage implements OnInit {

  @ViewChild("slide") slider : IonSlides

  slideIndex:number = 0;
  constructor(
    public navCtrl: NavController,
  ) {

  }

  ngOnInit() {
    this.slider.lockSwipes(true)
  }

  prev(){
    this.slider.lockSwipes(false);
    this.slider.slidePrev();
  }

  next(){
    this.slider.lockSwipes(false);
    this.slider.slideNext();
  }

  skip() {
    this.navCtrl.navigateRoot(["/home"]);
  }

  async handleSlideDone(e){
    this.slider.lockSwipes(false);
    this.slideIndex = await this.slider.getActiveIndex();
  }

  gotoHome(){
    localStorage.setItem('intro','false');
    this.navCtrl.navigateRoot(["/page-init"]);
  }
}
