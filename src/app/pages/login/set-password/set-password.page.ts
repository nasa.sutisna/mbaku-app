import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { ToastController, NavController } from '@ionic/angular';
import { RestApiService } from 'src/app/services/rest-api.service';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-set-password',
  templateUrl: './set-password.page.html',
  styleUrls: ['./set-password.page.scss'],
})
export class SetPasswordPage implements OnInit {
  isShow:boolean;
  isShowRetype:boolean;
  email:any;

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    public authService: AuthService,
    public toastCtrl: ToastController,
    public navCtrl: NavController,
    public restApi: RestApiService,
    public globalService: GlobalService
  ) { }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.email = params['email'];
    })
  }

  submit(form) {

    if(form.value.password != form.value.retypePassword){
      this.globalService.showToast('Konfirmasi password tidak cocok');
      return;
    }

    let params = {
      email: this.email
    }

    params = Object.assign(params,form.value)

    this.globalService.presentLoading();
    this.restApi.resetPassword(params).subscribe((result) => {
      this.globalService.showToast('Password berhasil diubah');
      this.navCtrl.navigateRoot('/login');
    }, error => {
      this.globalService.showToast(error.error.message);
    })
  }

  backButton() {
    this.navCtrl.pop();
  }
}
