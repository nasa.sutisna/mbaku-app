import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { ToastController, NavController } from '@ionic/angular';
import { RestApiService } from 'src/app/services/rest-api.service';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {

  constructor(
    public router: Router,
    public authService: AuthService,
    public toastCtrl: ToastController,
    public navCtrl: NavController,
    public restApi: RestApiService,
    public globalService: GlobalService
  ) { }

  ngOnInit() {
  }

  sendMail(form) {
    this.globalService.presentLoading();
    this.restApi.forgotPassword(form.value).subscribe((result:any) => {
      this.globalService.showToast(result.msg);
      this.globalService.dismissLoading();
      this.navCtrl.back()
    }, error => {
      this.globalService.showToast(error.error.msg);
      this.globalService.dismissLoading();
    })
  }

  backButton() {
    this.navCtrl.pop();
  }

}
