import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { ToastController, NavController } from '@ionic/angular';
import { RestApiService } from 'src/app/services/rest-api.service';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  isShow:boolean = false;
  constructor(
    public router: Router,
    public authService: AuthService,
    public toastCtrl: ToastController,
    public navCtrl: NavController,
    public restApi: RestApiService,
    public globalService: GlobalService
  ) { }

  ngOnInit() {
  }

  login(form) {
    this.globalService.presentLoading();
    this.authService.login(form.value)
      .then((result: any) => {
        
        this.globalService.dismissLoading();
        localStorage.setItem('authentication', JSON.stringify(result));
        localStorage.setItem('user', JSON.stringify(result.user.userInfo));

        this.authService.auth.emit(result);
        this.restApi.setAuth.next(result);

        if (result.user.userStatus == 1) {
          this.navCtrl.navigateRoot(['/home']);
        }
        else {
          this.navCtrl.navigateRoot(['/tabs/home']);
        }
      }).catch((error) => {
        this.globalService.showToast(error.error.msg);
        this.globalService.dismissLoading();
      })
  }

  openPage(page){
    this.router.navigateByUrl(page);
  }
  
  backButton() {
    this.navCtrl.pop();
  }
}
