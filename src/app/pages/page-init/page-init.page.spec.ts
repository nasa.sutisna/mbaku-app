import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageInitPage } from './page-init.page';

describe('PageInitPage', () => {
  let component: PageInitPage;
  let fixture: ComponentFixture<PageInitPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageInitPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageInitPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
