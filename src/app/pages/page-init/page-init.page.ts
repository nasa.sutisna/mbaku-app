import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-page-init',
  templateUrl: './page-init.page.html',
  styleUrls: ['./page-init.page.scss'],
})
export class PageInitPage implements OnInit {

  constructor(
    public navCtrl: NavController
  ) { }

  ngOnInit() {
  }

  gotoLogin(){
    this.navCtrl.navigateForward("/login");
  }

  gotoRegister(){
    this.navCtrl.navigateForward("/register");
  }
}
