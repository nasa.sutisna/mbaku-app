import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { NavService } from '../../services/nav.service';
import { RestApiService } from '../../services/rest-api.service';
import { AlertController, LoadingController, IonInfiniteScroll, NavController, Platform, Events } from '@ionic/angular';
import { FormControl } from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderOptions, NativeGeocoderResult } from '@ionic-native/native-geocoder/ngx';
import { StoragePathService } from 'src/app/services/storage-path.service';
import { Event } from '@angular/router';

declare const google;
declare const navigator;
@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;


  isLoading: boolean;
  loading: any;
  bookList: any[] = [];
  keyword = new FormControl();
  formData = {
    pageIndex: 0,
    pageSize: 6,
    category: [],
    keyword: '',
    sortBy: '',
    filterEbook: 0,
  }
  page: any = 1;
  totalPage = 1;
  images = ['Banner Yuk Pinjam Buku.png', 'Icon Banner Jatuh Tempo.png'];
  currentCity: any;
  currentProvince: any;
  currentLocation: any;
  nearbyList: any = [];
  MbakuAuth: any;
  memberID: any;
  banner: any;
  libraryPathPhoto: any;

  constructor(
    public navService: NavService,
    public navCtrl: NavController,
    public restApi: RestApiService,
    public alertCtrl: AlertController,
    public loadingController: LoadingController,
    public geolocation: Geolocation,
    public nativeGeocoder: NativeGeocoder,
    public platform: Platform,
    public storagePath: StoragePathService,
    public ngZone: NgZone,
    public events: Events
  ) { }

  ngOnInit() {
    this.getPathUrl();
    this.getCurrentLocation();
    this.getUserInfo();
    this.handleEvents();
  }

  ngOnDestroy(): void {
    this.events.unsubscribe('UPDATE:BANNER');
  }

  handleEvents() {
    this.events.subscribe('UPDATE:BANNER', (data) => {
      if (data) {
        this.getBanner();
      }
    })
  }

  getPathUrl() {
    this.libraryPathPhoto = this.storagePath.storageLibraryPhoto();
  }

  gotoDetail(code, book) {
    this.navService.push("book-detail", { kode_buku: code, book: book });
  }

  getUserInfo() {
    this.MbakuAuth = JSON.parse(localStorage.getItem('authentication'));
    this.memberID = this.MbakuAuth.user.userInfo.memberID;
    this.getBanner();
  }

  getCurrentLocation() {
    this.geolocation.getCurrentPosition().then((position) => {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };

      this.getAddressFromCoords(pos);
    })
      .catch(err => {
        console.log('err', err);
      })
  }

  filterBook(filter) {
    this.navCtrl.navigateForward(['/book'], { queryParams: { filterFromHome: filter } })
  }


  async getAddressFromCoords(position) {
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 2
    };

    this.isLoading = true;
    if (this.platform.is('cordova')) {
      this.nativeGeocoder.reverseGeocode(position.lat, position.lng, options)
        .then((result: NativeGeocoderResult[]) => {
          this.currentCity = result[0].subAdministrativeArea;
          this.currentProvince = result[0].administrativeArea;
          this.currentLocation = { city: this.currentCity, province: this.currentProvince };

          this.restApi.getNearbyLibrary(this.currentLocation)
            .subscribe((result) => {
              this.nearbyList = result;
              this.isLoading = false;
              this.getDistance(position, result);
            }, error => {
              this.isLoading = false;
            });
        }).catch(() => {
          this.isLoading = false;
        });
    }
    else {
      this.restApi.getNearbyLibrary(this.currentLocation)
        .subscribe((result) => {
          this.isLoading = false;
          this.nearbyList = result;
          this.getDistance(position, result);
        }, error => {
          this.isLoading = false;
        });
    }

  }

  getDistance(pos, nearbyList: any = []) {
    let getAddress = nearbyList.map((list) => { return list.libraryAddress });
    let getLatLong = nearbyList.map((list) => { return list.libraryLatLong });

    var origin1 = new google.maps.LatLng(pos);
    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix(
      {
        origins: [origin1],
        destinations: getAddress,
        travelMode: 'DRIVING',

      }, (response, status) => {
        this.ngZone.run(() => {
          for (let key in nearbyList) {
            nearbyList[key]['distance'] = response.rows[0]['elements'][key]['distance']['text'];
            nearbyList[key]['distance_number'] = response.rows[0]['elements'][key]['distance']['value'];
          }

          nearbyList.sort(function (a, b) {
            return parseFloat(a.distance_number) - parseFloat(b.distance_number);
          });
        })
      });
  }

  searchTitle() {
    this.navCtrl.navigateForward("/book");
  }

  getBanner() {
    this.restApi.getBanner(this.memberID)
      .subscribe((result) => {
        this.banner = result;
      })
  }

  gotoLibrary(nearby = false) {
    this.navCtrl.navigateForward(["/library"], { queryParams: { nearby: nearby } });
  }

  gotoDetailLibrary(list) {
    this.navCtrl.navigateForward(["/library-detail"], { queryParams: { data: JSON.stringify(list) } })
  }


  gotoDetailProfile() {
    this.navCtrl.navigateRoot('/tabs/profile');
  }
}


