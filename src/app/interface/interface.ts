
export interface loginInfo<T> {
    msg: string;
    user: { userInfo: T, userStatus: any };
    isLogin: any;
}

export interface pagination<T> {
    data: T;
    total: number;
    totalPage: number;
    page: number;
}

export interface getBanner<T> {
    isMemberPremium: boolean;
    borrow: T
}

export interface allowedRead {
    allowedRead: boolean;
    expireDate: any;
}
export interface ICheckRole {
    memberRole: number;
    memberSaldo: number;
    submission: string;
}

export interface IBookLoan {
    book: Array<any>;
    bookTotal: number;
    overdueDay: number;
    overDueFee: number;
}

export interface ILibrarySetting {
    status: number;
    message: any;
    librarySetting: ISetting;
}
export interface ISetting {
    libraryID: any;
    settingID: any;
    settingValue: ISettingValue;
}
export interface ISettingValue {
    calculateDueDate: string,
    dueDateFee: any,
    loanFee: any,
    operationalHours: any
}
export interface IResult {
    message: string
    status: number
}

export interface ICheckPaymentEbook {
    paymentPending: boolean;
    paymentToken: string;
}

export interface IResponseMidtrans {
    transaction_id: any;
    order_id: any;
    gross_amount: any;
    payment_type: any;
    transaction_time: any;
    transaction_status: any;
}
