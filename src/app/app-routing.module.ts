import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'ebook', loadChildren: './pages/ebook/ebook-viewer/ebook-viewer.module#EbookViewerPageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule' },
  { path: 'ebook', loadChildren: './pages/ebook/ebook.module#EbookPageModule' },
  { path: 'profile', loadChildren: './pages/profile/profile.module#ProfilePageModule' },
  { path: 'book', loadChildren: './pages/book/book.module#BookPageModule' },
  { path: 'home', loadChildren: './pages/admin/home/home.module#HomePageModule' },
  { path: 'onboarding', loadChildren: './pages/onboarding/onboarding.module#OnboardingPageModule' },
  { path: 'book-detail', loadChildren: './pages/book/book-detail/book-detail.module#BookDetailPageModule' },
  { path: 'ebook-detail', loadChildren: './pages/ebook/ebook-detail/ebook-detail.module#EbookDetailPageModule' },
  { path: 'ebook-viewer', loadChildren: './pages/ebook/ebook-viewer/ebook-viewer.module#EbookViewerPageModule' },
  { path: 'page-init', loadChildren: './pages/page-init/page-init.module#PageInitPageModule' },
  { path: 'book-search-title', loadChildren: './pages/book/book-search-title/book-search-title.module#BookSearchTitlePageModule' },
  { path: 'filter-book', loadChildren: './pages/book/filter-book/filter-book.module#FilterBookPageModule' },
  { path: 'categories', loadChildren: './pages/categories/categories.module#CategoriesPageModule' },
  { path: 'library', loadChildren: './pages/library/library.module#LibraryPageModule' },
  { path: 'filter-library-list', loadChildren: './pages/library/filter-library-list/filter-library-list.module#FilterLibraryListPageModule' },
  { path: 'library-detail', loadChildren: './pages/library/library-detail/library-detail.module#LibraryDetailPageModule' },
  { path: 'book-sorting', loadChildren: './pages/book/book-sorting/book-sorting.module#BookSortingPageModule' },
  { path: 'transactions', loadChildren: './pages/admin/transactions/transactions.module#TransactionsPageModule' },
  { path: 'ebook-filter', loadChildren: './pages/ebook/ebook-filter/ebook-filter.module#EbookFilterPageModule' },
  { path: 'profile-qrcode', loadChildren: './pages/profile/profile-qrcode/profile-qrcode.module#ProfileQrcodePageModule' },
  { path: 'profile-upgrade', loadChildren: './pages/profile/profile-upgrade/profile-upgrade.module#ProfileUpgradePageModule' },
  { path: 'profile-top-up', loadChildren: './pages/profile/profile-top-up/profile-top-up.module#ProfileTopUpPageModule' },  { path: 'profile-update', loadChildren: './pages/profile/profile-update/profile-update.module#ProfileUpdatePageModule' },
  { path: 'profile-history', loadChildren: './pages/profile/profile-history/profile-history.module#ProfileHistoryPageModule' },
  { path: 'profile-edit', loadChildren: './pages/profile/profile-edit/profile-edit.module#ProfileEditPageModule' },
  { path: 'setting', loadChildren: './pages/admin/setting/setting.module#SettingPageModule' },
  { path: 'book-list', loadChildren: './pages/admin/book/book-list/book-list.module#BookListPageModule' },
  { path: 'ebook-list', loadChildren: './pages/admin/ebook/ebook-list/ebook-list.module#EbookListPageModule' },
  { path: 'forgot-password', loadChildren: './pages/login/forgot-password/forgot-password.module#ForgotPasswordPageModule' },
  { path: 'set-password', loadChildren: './pages/login/set-password/set-password.module#SetPasswordPageModule' },






];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
