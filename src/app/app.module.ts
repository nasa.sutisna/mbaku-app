import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComponentsModule } from './components/components.module';

// SERVICES
import { NavService } from './services/nav.service';
import { RestApiService } from './services/rest-api.service';
import { AuthService } from './services/auth.service';
import { GlobalService } from './services/global.service';

// PLUGIN NATIVE
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { File } from '@ionic-native/file/ngx';
import { FileTransfer} from '@ionic-native/file-transfer/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { QRScanner } from '@ionic-native/qr-scanner/ngx';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { QRCodeModule } from 'angularx-qrcode';
import { Camera } from '@ionic-native/camera/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { FlexLayoutModule } from '@angular/flex-layout';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { AppAvailability } from '@ionic-native/app-availability/ngx';
import { Deeplinks } from '@ionic-native/deeplinks/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule, 
    QRCodeModule,
    HttpClientModule,
    IonicModule.forRoot(), 
    AppRoutingModule,
    ComponentsModule,
    FlexLayoutModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    NavService,
    RestApiService,
    AuthService,
    GlobalService,
    File,
    FileTransfer,
    AndroidPermissions,
    FileOpener,
    Geolocation,
    NativeGeocoder,
    QRScanner,
    BarcodeScanner,
    Camera,
    LocalNotifications,
    InAppBrowser,
    AppAvailability,
    Deeplinks,
    EmailComposer,
    CallNumber,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
