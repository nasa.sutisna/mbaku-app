import { Component, ViewChild, ViewChildren, QueryList, NgZone } from '@angular/core';

import { Platform, IonRouterOutlet, Events, ToastController, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { RestApiService } from './services/rest-api.service';
import { GlobalService } from './services/global.service';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { AuthService } from './services/auth.service';
import { StoragePathService } from './services/storage-path.service';
import { Deeplinks } from '@ionic-native/deeplinks/ngx';

declare var navigator: any;
declare var google: any;
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  @ViewChild(IonRouterOutlet) routerOutlet: IonRouterOutlet;
  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;
  intervalCheck: any;
  role: any;
  photoPath: any;

  adminMenu = [
    { label: 'Dashboard', icon: 'home', router: 'home' },
    { label: 'Peminjaman', icon: 'create', router: 'transactions', type: 'loan' },
    { label: 'Pengembalian', icon: 'create', router: 'transactions', type: 'loan-return' },
    { label: 'Buku', icon: 'book', router: 'book-list' },
    { label: 'Ebook', icon: 'albums', router: 'ebook-list' },
    { label: 'Pengaturan', icon: 'settings', router: 'setting' },
  ];

  user: any;
  auth: any;
  paymentPending: any;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private deeplinks: Deeplinks,
    public router: Router,
    public restApi: RestApiService,
    public events: Events,
    public toastCtrl: ToastController,
    public globalService: GlobalService,
    public localNotifications: LocalNotifications,
    public navCtrl: NavController,
    public authService: AuthService,
    public storagePath: StoragePathService,
    public ngZone: NgZone

  ) {
    this.authService.auth.subscribe(() => {
      this.user = JSON.parse(localStorage.getItem('user'));
    })

    this.initializeApp();
    this.handleEvents();
  }

  ngAfterViewInit() {
    this.platform.ready().then(() => {
      this.handleDeeplinks();
    })
  }

  ngOnDestroy(): void {
    clearInterval(this.intervalCheck);
  }

  handleEvents() {
    this.events.subscribe('MEMBER:RUNCHECKPENDINGPAYMENT', (token) => {
      if (token) {
        this.checkStatusPending();
      }
    })
  }

  handleDeeplinks() {
    this.deeplinks.route({
      '/resetpassword/:email/:memberID/:date': '/set-password'
    }).subscribe(match => {
      this.ngZone.run(() => {
        this.router.navigate([match.$route], { queryParams: match.$args }); // path would be like /auth/password/reset/123xyz
      });
      console.log('Successfully matched route', match);
    }, nomatch => {
      // nomatch.$link - the full link data
      console.error('Got a deeplink that didn\'t match', nomatch);
    });
  }

  checkStatusPending() {
    const params = {
      memberID: this.user.memberID
    }

    this.restApi.checkPendingPaymentTopUp(params).subscribe((result) => {
      this.paymentPending = result.paymentPending;
      if (result.paymentPending) {
        this.intervalCheck = setInterval(() => {
          this.checkPaymentPending(result.paymentToken);
        }, 10000)
      }
    })
  }

  checkPaymentPending(token) {
    const params = {
      id: token
    }

    this.restApi.getStatusPayment(params).subscribe((result: any) => {
      if (result.transaction_status != 'pending') {
        this.updatePaymentTopUp(result)
      }
    })
  }

  updatePaymentTopUp(params) {
    const key = {
      transaction_token: params.token,
      memberID: this.user.memberID
    }

    params = Object.assign(params, key)
    this.restApi.paymentTopUpUpdate(params).subscribe((result) => {
      if (params.transaction_status == 'settlement') {
        this.events.publish('MEMBER:UPDATEPROFILE');
        this.presentLocalNotif(1, 'TOP UP SALDO', `Hi ${this.user.memberFirstName}, Horee !!! Sekarang saldo MBAKU kamu sudah bertambah`);
      }

      clearInterval(this.intervalCheck);
    });
  }

  getPaymentStatus(token) {
    const params = {
      id: token
    }

    this.restApi.getStatusPayment(params).subscribe((result: any) => {
      console.log(result);
      if (result.transaction_status == 'settlement') {
        this.saveSaldo(result.token, result);
      }
    })
  }

  saveSaldo(token, params) {
    let criteria = {
      memberID: this.user.memberID,
      payment_token: token
    }

    criteria = Object.assign(criteria, params);

    this.restApi.paymentTopUp(criteria).subscribe((result) => {
      if (params.payment_type == 'bank_transfer' && params.transaction_status == 'settlement') {
        this.globalService.showToast('TopUp Saldo Berhasil', 'bottom', 5000);
        this.events.publish('MEMBER:UPDATEPROFILE', true);
      }

      this.navCtrl.navigateBack(['/tabs/profile']);
      clearInterval(this.intervalCheck);
    }, error => {
    })
  }

  initializeApp() {
    this.platform.ready().then(() => {

      this.auth = JSON.parse(localStorage.getItem('authentication'));
      this.user = JSON.parse(localStorage.getItem('user'));

      if (this.auth) {
        this.role = this.auth.user.userStatus;
      }

      if (this.auth) {
        if (this.auth.user.userInfo == 1) {
          this.photoPath = this.storagePath.storageProfile(this.user.staffID);
        }
        else {
          this.checkStatusPending();
        }
      }

      this.authService.role.subscribe((data: any) => {
        this.role = data.role;
      });

      this.statusBar.backgroundColorByHexString('#d4584e');
      let intro: any = localStorage.getItem("intro");
      let MBAKUAuth: any = localStorage.getItem("authentication");

      if (intro == null || intro == true) {
        this.router.navigateByUrl('/onboarding');
      }
      else if (MBAKUAuth == null) {
        this.router.navigateByUrl('/page-init');
      }
      else {
        if (this.role == 1) {
          this.router.navigateByUrl('/home');
        }
        else {
          this.router.navigateByUrl('/tabs/home');
        }
      }

      setTimeout(() => {
        this.splashScreen.hide();
      }, 1500);
      this.handleBackButton();
    });
  }

  handleBackButton() {
    this.platform.backButton.subscribeWithPriority(0, () => {
      if (this.routerOutlet && this.routerOutlet.canGoBack()) {
        this.routerOutlet.pop();
      }
      else if (
      this.router.url === '/tabs/home' 
      || this.router.url === '/tabs/profile' 
      || this.router.url === '/tabs/ebook'
      || this.router.url === '/onboarding' 
      || this.router.url === '/page-init' 
      || this.router.url === '/home'
      || this.router.url === '/book-list'
      || this.router.url === '/ebook-list'
      || this.router.url === '/setting'
      ) {
        // or if that doesn't work, try
        navigator['app'].exitApp();
      }
      else {
        console.log('else')
      }
    });
  }

  presentLocalNotif(id = 1, title, message) {
    if (this.platform.is('cordova')) {
      // Schedule a single notification
      this.localNotifications.schedule({
        id: id,
        title: title,
        text: message
      });
    }
  }

  logout() {
    localStorage.removeItem('authentication');
    localStorage.removeItem('user');
    this.authService.isLogin = false;
    this.navCtrl.navigateRoot("/page-init");
  }

  openPageProfile(route) {
    const user = JSON.parse(localStorage.getItem('user'));
    this.navCtrl.navigateForward([route], { queryParams: { id: user.memberID } })
  }

  openPage(route, type) {
    if (route == 'transactions') {
      this.navCtrl.navigateRoot([route], { queryParams: { type: type } });
    }
    else {
      this.navCtrl.navigateRoot([route]);
    }
  }
}
