import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { PDFDocumentProxy } from 'pdfjs-dist';
import { Router } from '@angular/router';
import { RestApiService } from 'src/app/services/rest-api.service';

@Component({
  selector: 'app-document-viewer',
  templateUrl: './document-viewer.component.html',
  styleUrls: ['./document-viewer.component.scss']
})
export class DocumentViewerComponent implements OnInit {

  page:any = 1;
  // totalPage:any = 0;
  urlBlob:any;
  screen:any;
  thumbsBlob:any;
  @Input() urlFile:any;
  @Input() ebookID:any;
  @Input() ratio:any;
  @Input() fileName:any;
  @Input() libraryID:any;
  @Output() totalPage: EventEmitter<any> = new EventEmitter<any>();
  @Output() isLoading: EventEmitter<any> = new EventEmitter<any>();
  @Output() handleError: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    public router: Router,
    public restApi: RestApiService
    
  ) { }

  ngOnInit() {
    this.screen = screen.width;
    this.ratio = 0.4;

    this.restApi.getFile(this.fileName,this.libraryID).subscribe((result) => {
      let reader = new FileReader();
      this.isLoading.emit('done');
      reader.onload = (e: any) => {
        this.urlBlob = e.target.result;
        this.thumbsBlob = e.target.result;
      };
       reader.readAsArrayBuffer(result);
    }, err =>{
      console.log(err);
      this.handleError.emit(true);
    })
  }

  setPage(event){
   this.page = event;
  }

  onProgress(e){
    if(e.loaded == e.total){
      // this.isLoading = false
    }
  }

  complete(pdf: PDFDocumentProxy){
    this.totalPage.emit(pdf.numPages);
  }

}
