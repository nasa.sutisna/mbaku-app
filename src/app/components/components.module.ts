import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DocumentViewerComponent } from './document-viewer/document-viewer.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { SidemenuComponent } from './sidemenu/sidemenu.component';
@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule,
    PdfViewerModule
  ],
  declarations: [
    DocumentViewerComponent,
    SidemenuComponent,
  ],
  exports: [
    DocumentViewerComponent,
    SidemenuComponent
  ]
})
export class ComponentsModule { }
