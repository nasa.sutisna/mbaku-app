import { Component, NgZone, ViewChildren, QueryList } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { NavController, IonRouterOutlet, Platform } from '@ionic/angular';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;

  isLogin: boolean;
  userInfo: any;
  activePage: any;

  constructor(
    public router: Router,
    public authService: AuthService,
    public navCtrl: NavController,
    public ngZone: NgZone,
    public platform: Platform
  ) {

  }

  ngOnInit() {
    this.activePage = this.router.url.substring(this.router.url.lastIndexOf('/') + 1);
    this.checkAuth();
    this.getUserInfo();
  }

  login() {
    this.navCtrl.navigateForward("/login");
  }

  checkAuth() {
    this.authService.auth.subscribe((data) => {
      this.isLogin = true;
      this.getUserInfo();
    });
  }


  getUserInfo() {
    let auth = JSON.parse(localStorage.getItem('authentication'));
    let user = JSON.parse(localStorage.getItem('user'));
    if (auth) {
      this.isLogin = auth.isLogin.status;
      this.userInfo = user;
      this.authService.isLogin = true;
    }
  }

  logout() {
    localStorage.removeItem('authentication');
    this.isLogin = false;
    this.authService.isLogin = false;
    this.navCtrl.navigateRoot("/tabs/home");
  }

  async openPage(page: string) {

    setTimeout(() => {

    }, 200)

    if (page == 'home') {
      await this.navCtrl.navigateRoot("/tabs/home");
    }
    else if (page == 'ebook') {
      await this.navCtrl.navigateRoot("/tabs/ebook");
    }
    else {
      await this.navCtrl.navigateRoot("/tabs/profile");
    }

    this.activePage = this.router.url.substring(this.router.url.lastIndexOf('/') + 1);

    console.log(this.router.url);
    console.log(this.activePage);

  }

}
